package uk.co.radical.android;


import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import io.reactivex.Single;
import io.reactivex.schedulers.TestScheduler;
import io.reactivex.subjects.PublishSubject;
import uk.co.radical.android.ui.onboarding.register.RegisterPresenter;
import uk.co.radical.android.util.MockVars;
import uk.co.radical.android.util.Tuple;
import uk.co.radicalapps.android.domain.usecase.RegisterUser;

import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static uk.co.radical.android.util.MockVars.EMPTY_STRING;
import static uk.co.radical.android.util.MockVars.INVALID_PASSWORD;
import static uk.co.radical.android.util.MockVars.VALID_EMAIL;
import static uk.co.radical.android.util.MockVars.VALID_PASSWORD;

public class RegisterPresenterTest {

    private PublishSubject<String> emailFieldPubSub = PublishSubject.create();
    private PublishSubject<String> passwordFieldPubSub = PublishSubject.create();
    private PublishSubject<Tuple<String, String>> registerButtonPubSub = PublishSubject.create();
    private PublishSubject<Boolean> connectivityPubSub = PublishSubject.create();

    @Mock private RegisterPresenter.View view;
    @Mock private RegisterUser registerUser;

    private RegisterPresenter presenter;
    private TestScheduler testScheduler;

    private void advanceScheduler(){
        testScheduler.advanceTimeBy(500, MILLISECONDS);
    }

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        testScheduler = new TestScheduler();

        when(view.getOnEmailTextChangeObs()).thenReturn(emailFieldPubSub);
        when(view.getOnPasswordTextChangeObs()).thenReturn(passwordFieldPubSub);
        when(view.getOnRegisterClickObs()).thenReturn(registerButtonPubSub);
        when(view.getConnectivityObservable()).thenReturn(connectivityPubSub);

        presenter = new RegisterPresenter(registerUser, testScheduler, testScheduler);
        presenter.attachView(view);

        //This represents the initial empty string sent when the edittext is instantiated
        emailFieldPubSub.onNext(EMPTY_STRING);
        passwordFieldPubSub.onNext(EMPTY_STRING);
        advanceScheduler();
    }

    @After
    public void tearDown(){
        presenter.detachView();
        presenter = null;
    }


    @Test
    public void testThatViewIsObservedOnAttach() throws Exception {
        verify(view, times(1)).getOnEmailTextChangeObs();
        verify(view, times(1)).getOnPasswordTextChangeObs();
        verify(view, times(1)).getOnRegisterClickObs();
        verify(view, times(1)).getConnectivityObservable();
        verify(view, times(1)).enableRegistrationButton(false);
        verifyNoMoreInteractions(view);
    }

    @Test
    public void testThatWhenEmailIncorrectErrorIsShown() throws Exception {
        emailFieldPubSub.onNext(MockVars.INVALID_EMAIL);

        advanceScheduler();

        verify(view, times(1)).showEmailError(true);
        verify(view, times(2)).enableRegistrationButton(false);
    }

    @Test
    public void testThatWhenEmailCorrectNoErrorIsShown() throws Exception {
        emailFieldPubSub.onNext(VALID_EMAIL);

        advanceScheduler();

        verify(view, times(1)).showEmailError(false);
        verify(view, times(2)).enableRegistrationButton(false);
    }

    @Test
    public void testThatWhenPasswordIncorrectErrorIsShown() throws Exception {
        passwordFieldPubSub.onNext(INVALID_PASSWORD);

        advanceScheduler();

        verify(view, times(1)).showPasswordError(true);
        verify(view, times(2)).enableRegistrationButton(false);
    }

    @Test
    public void testThatWhenPasswordCorrectNoErrorIsShown() throws Exception {
        passwordFieldPubSub.onNext(VALID_PASSWORD);

        advanceScheduler();

        verify(view, times(1)).showPasswordError(false);
        verify(view, times(2)).enableRegistrationButton(false);
    }

    @Test
    public void testThatWhenEmailAndPasswordCorrectNoErrorIsShownAndButtonIsEnabled() throws Exception {
        emailFieldPubSub.onNext(VALID_EMAIL);
        passwordFieldPubSub.onNext(VALID_PASSWORD);

        advanceScheduler();

        verify(view, times(1)).showEmailError(false);
        verify(view, times(1)).showPasswordError(false);
        verify(view, times(1)).enableRegistrationButton(true);
    }

    @Test
    public void testThatWhenEmailAndPasswordIncorrectErrorIsShownAndButtonIsDisabled() throws Exception {
        emailFieldPubSub.onNext(MockVars.INVALID_EMAIL);
        passwordFieldPubSub.onNext(INVALID_PASSWORD);

        advanceScheduler();

        verify(view, times(1)).showEmailError(true);
        verify(view, times(1)).showPasswordError(true);
        verify(view, times(3)).enableRegistrationButton(false);
    }

    @Test
    public void testThatWhenRegisterIsClickedWithSuccessfulResponseMoveToNextScreen(){
        String email = VALID_EMAIL;
        String password = VALID_PASSWORD;

        when(registerUser.execute(email, password)).thenReturn(Single.just(true));

        connectivityPubSub.onNext(true);
        emailFieldPubSub.onNext(email);
        passwordFieldPubSub.onNext(password);

        advanceScheduler();

        registerButtonPubSub.onNext(new Tuple<>(email, password));

        testScheduler.triggerActions();

        verify(view, never()).showEmailError(true);
        verify(view, never()).showPasswordError(true);
        verify(view, times(1)).showEmailError(false);
        verify(view, times(1)).showPasswordError(false);
        verify(view, times(3)).enableRegistrationButton(false);
        verify(view, times(2)).enableRegistrationButton(true);
        verify(view, times(1)).showLoading();
        verify(view, times(2)).hideLoading();
        verify(view).goToMainActivity();
    }

    @Test
    public void testThatWhenRegisterIsClickedWithNoInternetShowError(){
        String email = VALID_EMAIL;
        String password = VALID_PASSWORD;

        when(registerUser.execute(email, password)).thenReturn(Single.just(true));

        connectivityPubSub.onNext(false);
        emailFieldPubSub.onNext(email);
        passwordFieldPubSub.onNext(password);

        advanceScheduler();

        registerButtonPubSub.onNext(new Tuple<>(email, password));

        testScheduler.triggerActions();

        verify(view, never()).showEmailError(true);
        verify(view, never()).showPasswordError(true);
        verify(view, times(1)).showEmailError(false);
        verify(view, times(1)).showPasswordError(false);
        verify(view, times(3)).enableRegistrationButton(false);
        verify(view, times(2)).enableRegistrationButton(true);
        verify(view, times(1)).showLoading();
        verify(view, times(1)).onNoInternet();
    }
}
