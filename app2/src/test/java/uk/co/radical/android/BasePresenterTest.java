package uk.co.radical.android;


import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import io.reactivex.Scheduler;
import io.reactivex.Single;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import uk.co.radical.android.ui.base.BasePresenter;
import uk.co.radical.android.ui.base.BaseView;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.verifyZeroInteractions;

public class BasePresenterTest {

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Mock private BaseView view;
    private BasePresenter<BaseView> presenter;
    private TestBasePresenterExtension presenterExtension;

    @Before
    public void setUp(){
        MockitoAnnotations.initMocks(this);

        presenter = new BasePresenter<>(Schedulers.trampoline(), Schedulers.trampoline());
        presenterExtension = new TestBasePresenterExtension(Schedulers.trampoline(), Schedulers.trampoline());
    }


    @Test
    public void testOnAttachWithValidView(){
        presenter.attachView(view);

        verifyZeroInteractions(view);
    }

    @Test
    public void testOnAttachWithNullView(){
        thrown.expect(NullPointerException.class);
        thrown.expectMessage("View cannot be null");

        presenter.attachView(null);

        verifyZeroInteractions(view);
    }

    @Test
    public void testOnDetachAfterAttach(){
        presenter.attachView(view);

        try{
            presenter.detachView();
        } catch (Exception e){
            fail("Should not have thrown exception");
        }

        verifyZeroInteractions(view);
    }

    @Test
    public void testOnDetachBeforeAttach(){
        thrown.expect(IllegalStateException.class);
        thrown.expectMessage("Cannot call detachView() before attachView(view)");

        presenter.detachView();

        verifyZeroInteractions(view);
    }

    @Test
    public void testGetUiScheduler(){
        presenterExtension.attachView(view);

        Scheduler scheduler = presenterExtension.getUiSched();

        assertNotNull(scheduler);
        assertEquals(scheduler, Schedulers.trampoline());
    }

    @Test
    public void testGetIoScheduler(){
        presenterExtension.attachView(view);

        Scheduler scheduler = presenterExtension.getIoSched();

        assertNotNull(scheduler);
        assertEquals(scheduler, Schedulers.trampoline());
    }

    @Test
    public void testGetViewAfterAttach(){
        presenterExtension.attachView(view);

        BaseView view = presenterExtension.getBaseView();

        assertNotNull(view);
        assertEquals(view, this.view);
    }

    @Test
    public void testGetViewBeforeAttach(){
        thrown.expect(IllegalStateException.class);
        thrown.expectMessage("Cannot call getView() before attachView(view)");

        presenterExtension.getBaseView();
    }

    @Test
    public void testAddValidDisposable(){
        presenterExtension.attachView(view);

        try{
            presenterExtension.addSub(Single.just("").subscribe());
        } catch (Exception e){
            fail("Should not have thrown exception");
        }

        verifyZeroInteractions(view);
    }


    @Test
    public void testAddNullDisposable(){
        thrown.expect(NullPointerException.class);
        thrown.expectMessage("Disposable cannot be null");

        presenterExtension.addSub(null);
    }



    private class TestBasePresenterExtension extends BasePresenter<BaseView> {

        private TestBasePresenterExtension(Scheduler ioScheduler, Scheduler uiScheduler) {
            super(ioScheduler, uiScheduler);
        }

        private Scheduler getUiSched(){
            return getUiScheduler();
        }

        private Scheduler getIoSched(){
            return getIoScheduler();
        }

        private BaseView getBaseView(){
            return getView();
        }

        private void addSub(Disposable disposable){
            addSubscription(disposable);
        }
    }

}
