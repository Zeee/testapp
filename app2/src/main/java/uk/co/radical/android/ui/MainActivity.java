package uk.co.radical.android.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import uk.co.radical.android.R;
import uk.co.radical.android.VoucherApp;

public class MainActivity extends AppCompatActivity {

    public static Intent getIntent(Context context){
        return new Intent(context, MainActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        VoucherApp.getApp().getComponent().inject(this);
    }
}
