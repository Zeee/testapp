package uk.co.radical.android.ui.onboarding;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;

import butterknife.BindView;
import butterknife.ButterKnife;
import uk.co.radical.android.R;
import uk.co.radical.android.ui.base.BaseActivity;

public class OnboardingActivity extends BaseActivity {

    public static Intent getIntent(Context context){
        return new Intent(context, OnboardingActivity.class);
    }

    @BindView(R.id.onboarding_viewpager) ViewPager viewPager;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_onboarding);

        ButterKnife.bind(this);

        viewPager.setAdapter(new OnboardingPagerAdapter(getSupportFragmentManager()));
    }


    private static class OnboardingPagerAdapter extends FragmentPagerAdapter {

        OnboardingPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            if(position == 0){
                return OnboardingFragmentInfo.newInstance();
            } else if(position == 1){
                return OnboardingFragmentInfo.newInstance();
            } else {
                return AuthenticationFragment.newInstance();
            }
        }

        @Override
        public int getCount() {
            return 3;
        }
    }
}
