package uk.co.radical.android.di;


import javax.inject.Singleton;

import dagger.Component;
import uk.co.radical.android.di.modules.AppModule;
import uk.co.radical.android.di.modules.DataModule;
import uk.co.radical.android.di.modules.NetworkModule;
import uk.co.radical.android.di.modules.PresenterModule;
import uk.co.radical.android.di.modules.UseCaseModule;
import uk.co.radical.android.ui.MainActivity;
import uk.co.radical.android.ui.StartActivity;
import uk.co.radical.android.ui.onboarding.register.RegisterFragment;

@Singleton
@Component(modules = {AppModule.class, NetworkModule.class, DataModule.class, UseCaseModule.class, PresenterModule.class})
public interface AppComponent {

    void inject(MainActivity activity);
    void inject(StartActivity activity);
    void inject(RegisterFragment fragment);
}
