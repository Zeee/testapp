package uk.co.radical.android.ui.base;


import io.reactivex.Observable;

public interface NetworkView extends LoadDataView {
    void onNoInternet();
    void onTimeout();
    void onNetworkError(String error);
    Observable<Boolean> getConnectivityObservable();
}
