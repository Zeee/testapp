package uk.co.radical.android.ui.onboarding.register;

import android.app.ProgressDialog;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.github.pwittchen.reactivenetwork.library.rx2.ReactiveNetwork;
import com.jakewharton.rxbinding2.view.RxView;
import com.jakewharton.rxbinding2.widget.RxTextView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Observable;
import uk.co.radical.android.R;
import uk.co.radical.android.VoucherApp;
import uk.co.radical.android.ui.MainActivity;
import uk.co.radical.android.ui.base.BaseFragment;
import uk.co.radical.android.util.Tuple;


public class RegisterFragment extends BaseFragment implements RegisterPresenter.View {

    public static RegisterFragment newInstance() {

        Bundle args = new Bundle();

        RegisterFragment fragment = new RegisterFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Inject
    RegisterPresenter presenter;

    @BindView(R.id.register_button) Button registerButton;
    @BindView(R.id.register_email_address) EditText emailEditText;
    @BindView(R.id.register_password) EditText passwordEditText;
    private ProgressDialog progressDialog;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_register, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        VoucherApp.getApp().getComponent().inject(this);

        ButterKnife.bind(this, view);

        progressDialog = new ProgressDialog(view.getContext());
        progressDialog.setMessage(getResources().getString(R.string.registering));
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
    }

    @Override
    public void onStart() {
        super.onStart();
        presenter.attachView(this);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        presenter.detachView();
    }

    @Override
    public void onUnknownError(String error) {
        Toast.makeText(getContext(), error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onNoInternet() {
        Toast.makeText(getContext(), "No internet!", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onTimeout() {
        Toast.makeText(getContext(), "Timeout!", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onNetworkError(String error) {
        Toast.makeText(getContext(), error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public Observable<Boolean> getConnectivityObservable() {
        return ReactiveNetwork.observeNetworkConnectivity(emailEditText.getContext())
                .flatMap(connectivity -> Observable.just(connectivity.getState() == NetworkInfo.State.CONNECTED));
    }

    @Override
    public Observable<Tuple<String, String>> getOnRegisterClickObs() {
        return RxView.clicks(registerButton)
                .flatMap(o -> Observable.just(new Tuple<>(emailEditText.getText().toString(), passwordEditText.getText().toString())));
    }

    @Override
    public Observable<String> getOnEmailTextChangeObs() {
        return RxTextView.textChanges(emailEditText).map(String::valueOf);
    }

    @Override
    public Observable<String> getOnPasswordTextChangeObs() {
        return RxTextView.textChanges(passwordEditText).map(String::valueOf);
    }

    @Override
    public void goToMainActivity() {
        startActivity(MainActivity.getIntent(getContext()));
        if(getActivity() != null) {
            getActivity().finish();
        }
    }

    @Override
    public void enableRegistrationButton(boolean enable) {
        registerButton.setEnabled(enable);
    }

    @Override
    public void showEmailError(boolean show) {
        String error = show ? "Email is invalid" : null;
        emailEditText.setError(error);
    }

    @Override
    public void showPasswordError(boolean show) {
        String error = show ? "Password is invalid" : null;
        passwordEditText.setError(error);
    }

    @Override
    public void showLoading() {
        progressDialog.show();
    }

    @Override
    public void hideLoading() {
        progressDialog.dismiss();
    }
}
