package uk.co.radical.android;

import android.app.Application;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import timber.log.Timber;
import uk.co.radical.android.di.AppComponent;
import uk.co.radical.android.di.DaggerAppComponent;
import uk.co.radical.android.di.modules.AppModule;
import uk.co.radical.android.di.modules.DataModule;
import uk.co.radical.android.di.modules.NetworkModule;
import uk.co.radical.android.di.modules.PresenterModule;
import uk.co.radical.android.di.modules.UseCaseModule;


public class VoucherApp extends Application {

    private static VoucherApp app;
    private AppComponent component;

    public static VoucherApp getApp() {
        return app;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        app = this;

        configureLogging();

        configureRealm();

        configureDagger();
    }

    public AppComponent getComponent() {
        return component;
    }

    private void configureRealm(){
        Realm.init(this);

        RealmConfiguration configuration = new RealmConfiguration.Builder()
                .name("vc_db_realm")
                .schemaVersion(1)
                .build();

        Realm.setDefaultConfiguration(configuration);
    }

    private void configureLogging(){
        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        }
    }

    private void configureDagger(){
        component = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .networkModule(new NetworkModule(this, BuildConfig.BASE_URL, BuildConfig.OAUTH_CONSUMER_KEY, BuildConfig.OAUTH_CONSUMER_SECRET, BuildConfig.DEBUG))
                .dataModule(new DataModule(this))
                .useCaseModule(new UseCaseModule())
                .presenterModule(new PresenterModule())
                .build();
    }
}
