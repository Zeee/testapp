package uk.co.radical.android.di.modules;


import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import uk.co.radicalapps.android.domain.repository.OfferRepository;
import uk.co.radicalapps.android.domain.repository.UserRepository;
import uk.co.radicalapps.android.domain.usecase.GetOffers;
import uk.co.radicalapps.android.domain.usecase.CheckAuthenticated;
import uk.co.radicalapps.android.domain.usecase.RegisterUser;

@Module (includes = {DataModule.class})
public class UseCaseModule {

    @Provides
    @Singleton
    public RegisterUser provideRegisterUseCase(UserRepository userRepository){
        return new RegisterUser(userRepository);
    }

    @Provides
    @Singleton
    public CheckAuthenticated provideIsLoggedInUseCase(UserRepository userRepository){
        return new CheckAuthenticated(userRepository);
    }

    @Provides
    @Singleton
    public GetOffers provideGetOffers(OfferRepository offerRepository){
        return new GetOffers(offerRepository);
    }
}
