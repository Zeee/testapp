package uk.co.radical.android.util;

import java.util.Objects;

public class Tuple<L,R> {

    private final L left;
    private final R right;

    public Tuple(L left, R right) {
        this.left = left;
        this.right = right;
    }

    public L getLeft() { return left; }
    public R getRight() { return right; }

    @Override
    public int hashCode() {

        return left.hashCode() ^ right.hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof Tuple)) return false;
        Tuple pairo = (Tuple) o;
        return Objects.equals(left, pairo.getLeft()) && Objects.equals(right, pairo.getRight());
    }

}