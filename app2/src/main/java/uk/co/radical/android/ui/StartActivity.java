package uk.co.radical.android.ui;


import android.os.Bundle;
import android.support.annotation.Nullable;

import javax.inject.Inject;

import uk.co.radical.android.VoucherApp;
import uk.co.radical.android.ui.base.BaseActivity;
import uk.co.radical.android.ui.onboarding.OnboardingActivity;
import uk.co.radicalapps.android.domain.usecase.CheckAuthenticated;


public class StartActivity extends BaseActivity {

    @Inject
    CheckAuthenticated isUserLoggedIn;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        VoucherApp.getApp().getComponent().inject(this);

        isUserLoggedIn.execute()
                .subscribe(isLoggedIn -> {
                    if(isLoggedIn){
                        goToApp();
                    } else {
                        goToOnBoarding();
                    }
                    finish();
                });
    }

    private void goToOnBoarding(){
        startActivity(OnboardingActivity.getIntent(this));
    }

    private void goToApp(){
        startActivity(MainActivity.getIntent(this));
    }

}
