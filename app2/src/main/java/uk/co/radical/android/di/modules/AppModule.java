package uk.co.radical.android.di.modules;

import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import uk.co.radical.android.VoucherApp;

@Module
public class AppModule {

    private VoucherApp application;

    public AppModule(VoucherApp application) {
        this.application = application;
    }

    @Provides
    @Singleton
    VoucherApp provideApplication() {
        return application;
    }

    @Provides
    Context providesContext(){
        return application.getApplicationContext();
    }
}
