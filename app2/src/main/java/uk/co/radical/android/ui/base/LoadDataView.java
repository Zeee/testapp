package uk.co.radical.android.ui.base;


public interface LoadDataView extends BaseView {
    void showLoading();
    void hideLoading();
}
