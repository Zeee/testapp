package uk.co.radical.android.ui.base;


public interface IBasePresenter<V extends BaseView> {

    void attachView(V view);
    void detachView();
}
