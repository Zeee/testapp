package uk.co.radical.android.di.modules;

import android.content.Context;
import android.content.SharedPreferences;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.realm.Realm;
import uk.co.radicalapps.android.data.repository.OfferRepositoryImpl;
import uk.co.radicalapps.android.data.repository.UserRepositoryImpl;
import uk.co.radicalapps.android.data.repository.datasource.disk.Database;
import uk.co.radicalapps.android.data.repository.datasource.disk.DatabaseImpl;
import uk.co.radicalapps.android.data.repository.datasource.network.api.OfferApi;
import uk.co.radicalapps.android.data.repository.datasource.network.api.PreAuthApi;
import uk.co.radicalapps.android.data.repository.datasource.network.api.UserApi;
import uk.co.radicalapps.android.domain.repository.OfferRepository;
import uk.co.radicalapps.android.domain.repository.UserRepository;

@Module (includes = {NetworkModule.class})
public class DataModule {

    private Context context;

    public DataModule(Context context) {
        this.context = context;
    }

    @Provides
    @Singleton
    public Realm provideRealm(){
        return Realm.getDefaultInstance();
    }

    @Provides
    @Singleton
    public SharedPreferences provideSharedPreferences(){
        return context.getSharedPreferences("VC_SHARED_PREFS", Context.MODE_PRIVATE);
    }

    @Provides
    @Singleton
    public Database providesDatabase(SharedPreferences sharedPreferences, Realm realm){
        return new DatabaseImpl(sharedPreferences, realm);
    }

    @Provides
    @Singleton
    OfferRepository provideOfferRepository(OfferApi offerApi, Database database){
        return new OfferRepositoryImpl(offerApi, database);
    }

    @Provides
    @Singleton
    UserRepository provideUserRepository(PreAuthApi preAuthApi, UserApi userApi, Database database){
        return new UserRepositoryImpl(preAuthApi, userApi, database);
    }

}
