package uk.co.radical.android.observer;

import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.net.SocketTimeoutException;

import io.reactivex.observers.DisposableObserver;
import okhttp3.ResponseBody;
import retrofit2.HttpException;
import retrofit2.Response;
import uk.co.radical.android.exception.NoNetworkException;
import uk.co.radical.android.ui.base.NetworkView;


public abstract class NetworkRequestObserver<T> extends DisposableObserver<T>{

    private WeakReference<NetworkView> weakReference;

    protected NetworkRequestObserver(NetworkView view) {
        this.weakReference = new WeakReference<>(view);
    }

    protected abstract void onSuccess(T t);

    @Override
    protected void onStart() {
        super.onStart();
        NetworkView view = weakReference.get();
        view.showLoading();
    }

    @Override
    public void onNext(T t) {
        NetworkView view = weakReference.get();
        view.hideLoading();
        onSuccess(t);
    }

    @Override
    public void onError(Throwable e) {
        NetworkView view = weakReference.get();

        view.hideLoading();

        if (e instanceof HttpException) {
            view.onNetworkError(getErrorMessage((HttpException) e));
        } else if (e instanceof SocketTimeoutException) {
            view.onTimeout();
        } else if(e instanceof NoNetworkException){
            view.onNoInternet();
        } else {
            view.onUnknownError(e.getMessage());
        }
    }

    @Override
    public void onComplete() {
        NetworkView view = weakReference.get();
        view.hideLoading();
    }

    private String getErrorMessage(HttpException httpException) {
        Response response = httpException.response();
        ResponseBody responseBody = response.errorBody();
        try {
            JSONObject jsonObject = new JSONObject(responseBody != null ? responseBody.string() : "");
            return jsonObject.getString("description");
        } catch (Exception e) {
            return e.getMessage();
        }
    }
}
