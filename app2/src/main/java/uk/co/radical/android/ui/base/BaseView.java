package uk.co.radical.android.ui.base;


public interface BaseView {
    void onUnknownError(String error);
}
