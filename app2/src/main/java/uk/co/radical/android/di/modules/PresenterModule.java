package uk.co.radical.android.di.modules;


import dagger.Module;
import dagger.Provides;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import uk.co.radical.android.ui.onboarding.register.RegisterPresenter;
import uk.co.radicalapps.android.domain.usecase.RegisterUser;

@Module (includes = {UseCaseModule.class})
public class PresenterModule {

    @Provides
    public RegisterPresenter provideRegisterPresenter(RegisterUser registerUser){
        return new RegisterPresenter(registerUser, Schedulers.io(), AndroidSchedulers.mainThread());
    }


}
