package uk.co.radical.android.ui.onboarding.register;


import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.Scheduler;
import io.reactivex.disposables.Disposable;
import io.reactivex.subjects.BehaviorSubject;
import uk.co.radical.android.exception.NoNetworkException;
import uk.co.radical.android.observer.NetworkRequestObserver;
import uk.co.radical.android.ui.base.BasePresenter;
import uk.co.radical.android.ui.base.NetworkView;
import uk.co.radical.android.util.Tuple;
import uk.co.radicalapps.android.domain.ValidationUtils;
import uk.co.radicalapps.android.domain.usecase.RegisterUser;

public class RegisterPresenter extends BasePresenter<RegisterPresenter.View> {

    private RegisterUser registerUser;
    private BehaviorSubject<Boolean> isConnectedSubject;

    public RegisterPresenter(RegisterUser registerUser, Scheduler ioScheduler, Scheduler uiScheduler) {
        super(ioScheduler, uiScheduler);
        this.registerUser = registerUser;
        isConnectedSubject = BehaviorSubject.create();
    }

    @Override
    public void attachView(View view) {
        super.attachView(view);

        addSubscription(view.getConnectivityObservable()
                .subscribe(isConnected -> isConnectedSubject.onNext(isConnected)));

        addSubscription(getFormValidationDisposable(view));

        addSubscription(view.getOnRegisterClickObs().subscribe(tuple -> registerUser(tuple.getLeft(), tuple.getRight())));

    }


    private void registerUser(String email, String password){
        isConnectedSubject.first(false).toObservable()
            .switchMap(isConnected -> {
                if(!isConnected) throw new NoNetworkException();

                return registerUser.execute(email, password).toObservable();
            })
            .subscribeOn(getIoScheduler())
            .observeOn(getUiScheduler())
            .doOnSubscribe(disposable -> getView().enableRegistrationButton(false))
            .doOnTerminate(() -> getView().enableRegistrationButton(true))
            .subscribe(new NetworkRequestObserver<Boolean>(getView()) {
                @Override
                protected void onSuccess(Boolean unused) {
                    getView().goToMainActivity();
                }
            });
    }


    private Disposable getFormValidationDisposable(View view){
        Observable<Boolean> emailObs = view.getOnEmailTextChangeObs()
                .debounce(500, TimeUnit.MILLISECONDS, getIoScheduler())
                .map(ValidationUtils::isEmailValid);

        Observable<Boolean> passwordObs = view.getOnPasswordTextChangeObs()
                .debounce(500, TimeUnit.MILLISECONDS, getIoScheduler())
                .map(ValidationUtils::isPasswordValid);

        emailObs
                .skip(1)
                .observeOn(getUiScheduler())
                .subscribe(isValid -> getView().showEmailError(!isValid));

        passwordObs
                .skip(1)
                .observeOn(getUiScheduler())
                .subscribe(isValid -> getView().showPasswordError(!isValid));

        return Observable
                .combineLatest(
                        emailObs,
                        passwordObs,
                        (isEmailValid, isPasswordValid) -> isEmailValid && isPasswordValid)
                .observeOn(getUiScheduler())
                .subscribe(enable -> getView().enableRegistrationButton(enable));
    }


    public interface View extends NetworkView {
        Observable<Tuple<String, String>> getOnRegisterClickObs();
        Observable<String> getOnEmailTextChangeObs();
        Observable<String> getOnPasswordTextChangeObs();

        void goToMainActivity();
        void enableRegistrationButton(boolean enable);
        void showEmailError(boolean show);
        void showPasswordError(boolean show);
    }
}
