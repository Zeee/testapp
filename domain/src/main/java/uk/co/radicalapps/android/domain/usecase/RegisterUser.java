package uk.co.radicalapps.android.domain.usecase;


import io.reactivex.Single;
import uk.co.radicalapps.android.domain.repository.UserRepository;

public class RegisterUser {

    private UserRepository repository;

    public RegisterUser(UserRepository repository) {
        this.repository = repository;
    }

    public Single<Boolean> execute(String username, String password) {
        return repository.generateToken()
                .flatMap(authToken -> repository.authoriseToken(authToken))
                .flatMap(unUsed -> repository.fetchMetadata())
                .flatMap(unUsed -> repository.registerNewUser(username, password))
                .map(user -> {
                    repository.saveUser(user);
                    repository.setUserAuthenticated();
                    return true;
                });
    }
}
