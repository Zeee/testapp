package uk.co.radicalapps.android.domain;


import java.util.regex.Pattern;

public class ValidationUtils {

    private static final Pattern EMAIL_ADDRESS_REGEX
            = Pattern.compile(
            "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" +
                    "\\@" +
                    "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
                    "(" +
                    "\\." +
                    "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" +
                    ")+"
    );

    public static boolean isEmailValid(String email){
        return EMAIL_ADDRESS_REGEX.matcher(email).matches();
    }

    public static boolean isPasswordValid(String password){
        return password != null && password.length() >= 8;
    }
}
