package uk.co.radicalapps.android.domain.usecase;


import io.reactivex.Observable;
import uk.co.radicalapps.android.domain.data.Offer;
import uk.co.radicalapps.android.domain.repository.OfferRepository;

public class SaveOffer {

    private OfferRepository offerRepository;

    public SaveOffer(OfferRepository offerRepository) {
        this.offerRepository = offerRepository;
    }

    public Observable<Void> execute() {
        offerRepository.saveOffer(new Offer(System.currentTimeMillis()+""));
        return Observable.empty();
    }
}
