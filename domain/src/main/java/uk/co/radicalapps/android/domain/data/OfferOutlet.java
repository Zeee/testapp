package uk.co.radicalapps.android.domain.data;


public class OfferOutlet {

    private String offerId;
    private String outletId;

    public OfferOutlet(String offerId, String outletId) {
        this.offerId = offerId;
        this.outletId = outletId;
    }

    public String getOfferId() {
        return offerId;
    }

    public String getOutletId() {
        return outletId;
    }
}
