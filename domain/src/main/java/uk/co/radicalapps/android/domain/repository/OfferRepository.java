package uk.co.radicalapps.android.domain.repository;


import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Single;
import uk.co.radicalapps.android.domain.data.Offer;
import uk.co.radicalapps.android.domain.data.OfferOutlet;

public interface OfferRepository {

    Flowable<List<Offer>> getOffers();
    Single<Void> saveOfferOutlet(OfferOutlet offerOutlet);
    Single<List<Offer>> getSavedOffers();
    void saveOffer(Offer offer);

}
