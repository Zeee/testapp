package uk.co.radicalapps.android.domain.data;


import java.util.List;

public interface User {

    String getUserID();
    User setUserID(String userId);

    String getFirstName();
    User setFirstName(String firstName);

    String getLastName();
    User setLastName(String lastName);

    String getEmailAddress();
    User setEmailAddress(String emailAddress);

    String getPostCode();
    User setPostCode(String postCode);

    List<String> getFavouriteMerchants();
    User setFavouriteMerchants(List<String> favouriteMerchantIds);
}
