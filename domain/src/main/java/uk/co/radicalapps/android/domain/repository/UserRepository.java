package uk.co.radicalapps.android.domain.repository;


import io.reactivex.Single;
import uk.co.radicalapps.android.domain.data.User;

public interface UserRepository {

    Single<String> generateToken();
    Single<Object> authoriseToken(String authToken);
    Single<Boolean> fetchMetadata();
    Single<User> registerNewUser(String email, String password);
    Single<User> getUserInfo();
    Single<Boolean> resetPassword(String email);
    Single<String> checkEmailStatus(String email);
    Single<User> login(String email, String password);
    Single<Boolean> isUserAuthenticated();
    Single<Boolean> hasUserSkippedSignUp();

    void saveUser(User user);
    void setUserAuthenticated();
    void setUserSkippedSignUp();

    Single<User> socialLogin(String socialUuid, String gigSIG, String timestamp);
}
