package uk.co.radicalapps.android.domain.data;


public class Offer {

    String offerId;
    OfferOutlet offerOutlet;

    public Offer(String offerId) {
        this.offerId = offerId;
    }

    public String getOfferId() {
        return offerId;
    }

    public OfferOutlet getOfferOutlet() {
        return offerOutlet;
    }

    public void setOfferOutlet(OfferOutlet offerOutlet) {
        this.offerOutlet = offerOutlet;
    }
}
