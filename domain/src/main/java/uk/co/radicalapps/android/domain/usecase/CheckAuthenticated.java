package uk.co.radicalapps.android.domain.usecase;


import io.reactivex.Single;
import uk.co.radicalapps.android.domain.repository.UserRepository;

public class CheckAuthenticated {

    private UserRepository userRepository;

    public CheckAuthenticated(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public Single<Boolean> execute(){
        return Single.zip(userRepository.isUserAuthenticated(), userRepository.hasUserSkippedSignUp(),
                (isAuthenticated, hasSkippedSignUp) -> isAuthenticated|| hasSkippedSignUp);
    }
}
