package uk.co.radicalapps.android.domain.usecase;


import java.util.List;

import io.reactivex.Flowable;
import uk.co.radicalapps.android.domain.data.Offer;
import uk.co.radicalapps.android.domain.repository.OfferRepository;

public class GetOffers {

    private OfferRepository offerRepository;

    public GetOffers(OfferRepository offerRepository) {
        this.offerRepository = offerRepository;
    }

    public Flowable<List<Offer>> execute(){
        return offerRepository.getOffers();
    }

}
