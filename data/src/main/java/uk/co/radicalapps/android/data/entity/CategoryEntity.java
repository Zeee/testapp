package uk.co.radicalapps.android.data.entity;


import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;

public class CategoryEntity extends RealmObject {

    @PrimaryKey
    @Required
    private String categoryID;
    private String categoryName;


}
