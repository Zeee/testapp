package uk.co.radicalapps.android.data.entity.mapper;


import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import uk.co.radicalapps.android.data.entity.OfferEntity;
import uk.co.radicalapps.android.domain.data.Offer;

public class OfferEntityMapper {

    public static Offer transform(OfferEntity offerEntity){
        return new Offer(offerEntity.getOfferID());
    }

    public static List<Offer> transform(List<OfferEntity> offerEntityList){
        if(offerEntityList == null || offerEntityList.isEmpty()){
            return Collections.emptyList();
        }

        ArrayList<Offer> offers = new ArrayList<>(offerEntityList.size());
        for(OfferEntity userInfoEntity : offerEntityList){
            offers.add(transform(userInfoEntity));
        }
        return offers;
    }

    public static OfferEntity transform(Offer offer){
        OfferEntity offerEntity = new OfferEntity();
        offerEntity.setOfferID(offer.getOfferId());
        return offerEntity;
    }
}
