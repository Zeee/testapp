package uk.co.radicalapps.android.data.repository.datasource.network.auth;
/*
 * Copyright (C) 2013 Patrik Åkerfeldt
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import oauth.signpost.http.HttpRequest;
import okhttp3.Headers;
import okhttp3.MediaType;
import okhttp3.Request;


/**
 * Adapts the OKHTTP request to the HTTPRequest interface so we can use the
 * signpost base oAuth consumer class to sign the requests using oAuth
 */
public class HttpRequestAdapter implements HttpRequest {

    private static final MediaType DEFAULT_CONTENT_TYPE = MediaType.parse("application/json; charset=utf-8");

    private Request request;

    private MediaType contentType;

    public HttpRequestAdapter(Request request) {
        this(request, request.body() != null ? request.body().contentType() : null);

        if (contentType == null) {
            contentType = DEFAULT_CONTENT_TYPE;
        }
    }

    public HttpRequestAdapter(Request request, MediaType contentType) {
        this.request = request;
        this.contentType = contentType;
    }

    @Override
    public Map<String, String> getAllHeaders() {
        HashMap<String, String> headers = new HashMap<>();
        Headers requestHeaders = request.headers();
        int index = 0;
        for (int i = 0; i < requestHeaders.size(); i++) {
            String name = requestHeaders.name(index);
            String value = requestHeaders.value(index);
            headers.put(name, value);
        }
        return headers;
    }

    @Override
    public String getContentType() {
        if (contentType != null) {
            return contentType.toString();
        }
        return "";
    }

    @Override
    public String getHeader(String key) {
        return request.header(key);
    }

    @Override
    public InputStream getMessagePayload() throws IOException {
        okio.Buffer buffer = new okio.Buffer();
        request.body().writeTo(buffer);
        return buffer.inputStream();
    }

    @Override
    public String getMethod() {
        return request.method();
    }

    @Override
    public String getRequestUrl() {
        return request.url().url().toString();
    }

    @Override
    public void setHeader(String key, String value) {
        Request.Builder requestBuilder = request.newBuilder();
        requestBuilder.header(key, value);
        request = requestBuilder.build();
    }

    @Override
    public void setRequestUrl(String url) {
        Request.Builder requestBuilder = request.newBuilder();
        requestBuilder.url(url);
        request = requestBuilder.build();
    }

    @Override
    public Object unwrap() {
        return request;
    }

}