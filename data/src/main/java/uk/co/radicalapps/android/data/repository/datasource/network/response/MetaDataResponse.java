package uk.co.radicalapps.android.data.repository.datasource.network.response;


import com.google.gson.annotations.SerializedName;

import java.util.List;

import uk.co.radicalapps.android.data.entity.CategoryEntity;
import uk.co.radicalapps.android.data.entity.MerchantEntity;
import uk.co.radicalapps.android.data.entity.PopularSearchEntity;


public class MetaDataResponse {

    @SerializedName("InstoreCategory")
    List<CategoryEntity> instoreCategoryList;
    @SerializedName("OnlineCategory")
    List<CategoryEntity> onlineCategoryList;
    @SerializedName("PopularSearches")
    List<PopularSearchEntity> popularSearchList;
    @SerializedName("PopularMerchants")
    List<MerchantEntity> popularMerchantList;

    public List<CategoryEntity> getInstoreCategoryList() {
        return instoreCategoryList;
    }

    public List<CategoryEntity> getOnlineCategoryList() {
        return onlineCategoryList;
    }

    public List<PopularSearchEntity> getPopularSearchList() {
        return popularSearchList;
    }

    public List<MerchantEntity> getPopularMerchantList() {
        return popularMerchantList;
    }
}
