package uk.co.radicalapps.android.data.entity;


import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;

public class PopularSearchEntity extends RealmObject {

    @PrimaryKey
    @Required
    String searchName;
    SearchHintEntity searchHint;
}
