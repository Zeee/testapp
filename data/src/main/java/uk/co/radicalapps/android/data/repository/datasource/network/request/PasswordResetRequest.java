package uk.co.radicalapps.android.data.repository.datasource.network.request;


public class PasswordResetRequest {

    private String email;

    public PasswordResetRequest(String email) {
        this.email = email;
    }
}
