package uk.co.radicalapps.android.data.repository.datasource.network.api;


import io.reactivex.Single;
import retrofit2.http.Body;
import retrofit2.http.POST;
import uk.co.radicalapps.android.data.repository.datasource.network.request.ReportingClientErrorRequest;
import uk.co.radicalapps.android.data.repository.datasource.network.request.ReportingRequest;
import uk.co.radicalapps.android.data.repository.datasource.network.request.ReportingUserActionRequest;

public interface ReportingApi {

    @POST("reporting/clienterror")
    Single<Void> reportClientError(@Body ReportingClientErrorRequest reportingClientErrorRequest);

    @POST("reporting/openpushnotification")
    Single<Void> reportPushNotificationOpened(@Body ReportingRequest reportingRequest);

    @POST("reporting/redeemoffer")
    Single<Void> reportOfferRedeemed(@Body ReportingRequest reportingRequest);

    @POST("reporting/savedoffer")
    Single<Void> reportOfferSaved(@Body ReportingRequest reportingRequest);

    @POST("reporting/viewoffer")
    Single<Void> reportOfferViewed(@Body ReportingRequest reportingRequest);

    @POST("reporting/viewpage")
    Single<Void> reportPageViewed(@Body ReportingRequest reportingRequest);

    @POST("reporting/getcodefail")
    Single<Void> reportGetCodeFailed(@Body ReportingRequest reportingRequest);

    @POST("reporting/useraction")
    Single<Void> reportUserAction(@Body ReportingUserActionRequest reportingUserActionRequest);

}
