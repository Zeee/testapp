package uk.co.radicalapps.android.data.repository.datasource.network.api;


import io.reactivex.Single;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import uk.co.radicalapps.android.data.repository.datasource.network.request.CheckEmailRequest;
import uk.co.radicalapps.android.data.repository.datasource.network.request.LoginRequest;
import uk.co.radicalapps.android.data.repository.datasource.network.request.PasswordResetRequest;
import uk.co.radicalapps.android.data.repository.datasource.network.request.SocialLoginRequest;
import uk.co.radicalapps.android.data.repository.datasource.network.request.UserSignupRequest;
import uk.co.radicalapps.android.data.repository.datasource.network.response.CheckEmailResponse;
import uk.co.radicalapps.android.data.repository.datasource.network.response.PasswordResetResponse;
import uk.co.radicalapps.android.data.repository.datasource.network.response.UserInfoResponse;

public interface UserApi {

    @GET("user/info")
    Single<UserInfoResponse> getUserInfo();

    @POST("user/signup")
    Single<UserInfoResponse> signUp(@Body UserSignupRequest userSignupRequest);

    @POST("user/sociallogin")
    Single<UserInfoResponse> socialLogin(@Body SocialLoginRequest socialLoginRequest);

    @POST("user/login")
    Single<UserInfoResponse> login(@Body LoginRequest loginRequest);

    @POST("user/passwordreset")
    Single<PasswordResetResponse> resetPassword(@Body PasswordResetRequest passwordResetRequest);

    @POST("user/checkemail")
    Single<CheckEmailResponse> checkUserEmailStatus(@Body CheckEmailRequest checkEmailRequest);



}
