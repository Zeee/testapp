package uk.co.radicalapps.android.data.repository;


import java.util.ArrayList;
import java.util.List;

import io.reactivex.Single;
import uk.co.radicalapps.android.data.entity.CategoryEntity;
import uk.co.radicalapps.android.data.entity.MerchantEntity;
import uk.co.radicalapps.android.data.entity.PopularSearchEntity;
import uk.co.radicalapps.android.data.entity.UserEntity;
import uk.co.radicalapps.android.data.entity.mapper.UserEntityMapper;
import uk.co.radicalapps.android.data.repository.datasource.disk.Database;
import uk.co.radicalapps.android.data.repository.datasource.network.api.PreAuthApi;
import uk.co.radicalapps.android.data.repository.datasource.network.api.UserApi;
import uk.co.radicalapps.android.data.repository.datasource.network.request.AuthorisationRequest;
import uk.co.radicalapps.android.data.repository.datasource.network.request.CheckEmailRequest;
import uk.co.radicalapps.android.data.repository.datasource.network.request.LoginRequest;
import uk.co.radicalapps.android.data.repository.datasource.network.request.PasswordResetRequest;
import uk.co.radicalapps.android.data.repository.datasource.network.request.SocialLoginRequest;
import uk.co.radicalapps.android.data.repository.datasource.network.request.UserSignupRequest;
import uk.co.radicalapps.android.data.repository.datasource.network.response.CheckEmailResponse;
import uk.co.radicalapps.android.data.repository.datasource.network.response.UserInfoResponse;
import uk.co.radicalapps.android.domain.data.User;
import uk.co.radicalapps.android.domain.repository.UserRepository;

public class UserRepositoryImpl implements UserRepository {

    private PreAuthApi preAuthApi;
    private UserApi userApi;
    private Database database;

    public UserRepositoryImpl(PreAuthApi preAuthApi, UserApi userApi, Database database) {
        this.preAuthApi = preAuthApi;
        this.userApi = userApi;
        this.database = database;
    }

    @Override
    public Single<Boolean> isUserAuthenticated(){
        return Single.just(database.isUserLoggedIn());
    }

    @Override
    public Single<Boolean> hasUserSkippedSignUp(){
        return Single.just(database.hasUserSkippedSignUp());
    }

    @Override
    public void saveUser(User user) {
        database.saveUserData(UserEntityMapper.transform(user));
    }

    @Override
    public void setUserAuthenticated() {
        database.setUserLoggedIn();
    }

    @Override
    public void setUserSkippedSignUp() {
        database.setUserSkippedSignUp();
    }

    @Override
    public Single<String> generateToken(){
        return preAuthApi.generateToken()
                .map(tokenResponse -> {
                    if(tokenResponse == null || tokenResponse.getToken() == null || tokenResponse.getToken().isEmpty()){
                        throw new NullPointerException("Auth token is null");
                    }

                    return tokenResponse.getToken();
                });
    }

    @Override
    public Single<Object> authoriseToken(String authToken){
        return preAuthApi.authoriseToken(new AuthorisationRequest(authToken))
                .map(authorisationResponse -> {
                    if(authorisationResponse == null || authorisationResponse.getOauthToken() == null || authorisationResponse.getOauthTokenSecret() == null){
                        throw new NullPointerException("Client authentication failed");
                    }

                    String oAuthToken = authorisationResponse.getOauthToken();
                    String oAuthTokenSecret = authorisationResponse.getOauthTokenSecret();

                    database.saveAuthToken(oAuthToken);
                    database.saveAuthTokenSecret(oAuthTokenSecret);

                    return new Object();
                });
    }

    @Override
    public Single<Boolean> fetchMetadata() {
        return preAuthApi.getAppMetaData()
                .map(metaDataResponse -> {
                    if(metaDataResponse == null || metaDataResponse.getOnlineCategoryList() == null || metaDataResponse.getInstoreCategoryList() == null
                            || metaDataResponse.getPopularMerchantList() == null || metaDataResponse.getPopularSearchList() == null){
                        throw new NullPointerException("Incomplete meta-data was returned");
                    }

                    List<CategoryEntity> inStoreCategories = metaDataResponse.getInstoreCategoryList();
                    List<CategoryEntity> onlineCategories = metaDataResponse.getOnlineCategoryList();
                    List<MerchantEntity> merchantList = metaDataResponse.getPopularMerchantList();
                    List<PopularSearchEntity>  searches = metaDataResponse.getPopularSearchList();

                    List<CategoryEntity> categories = new ArrayList<>();
                    categories.addAll(inStoreCategories);
                    categories.addAll(onlineCategories);

                    database.saveCategoriesAsync(categories);
                    database.saveMerchantsAsync(merchantList);
                    database.savePopularSearchesAsync(searches);

                    return true;
                });
    }

    @Override
    public Single<User> registerNewUser(String email, String password){
        return userApi.signUp(new UserSignupRequest("name", email, password))
                .map(userInfoResponse -> {
                    if(userInfoResponse != null && userInfoResponse.getUserInfo() != null){
                        return userInfoResponse.getUserInfo();
                    } else {
                        throw new NullPointerException("Received invalid data from backend");
                    }
                });
    }

    @Override
    public Single<User> login(String email, String password){
        LoginRequest request = new LoginRequest(email, password);
        return userApi.login(request)
                .map(userInfoResponse -> {
                    UserEntity userInfoEntity = userInfoResponse.getUserInfo();
                    database.saveUserData(userInfoEntity);

                    return userInfoEntity;
                });
    }

    @Override
    public Single<User> getUserInfo(){
        Single<UserEntity> userInfoLocal = database.getUserInfo();
        Single<UserEntity> userInfoNetwork = userApi.getUserInfo().map(UserInfoResponse::getUserInfo);

        return Single.concat(userInfoLocal, userInfoNetwork)
                .firstOrError()
                .map(userInfoEntity -> {
                    database.saveUserData(userInfoEntity);
                    return userInfoEntity;
                });
    }

    @Override
    public Single<Boolean> resetPassword(String email){
        PasswordResetRequest request = new PasswordResetRequest(email);
        return userApi.resetPassword(request)
                .map(passwordResetResponse -> {
                    boolean isSuccess = passwordResetResponse.isSuccess();
                    if(isSuccess) {
                        return true;
                    } else {
                        throw new Exception(passwordResetResponse.getMessage());
                    }
                });
    }

    @Override
    public Single<String> checkEmailStatus(String email){
        CheckEmailRequest request = new CheckEmailRequest(email);
        return userApi.checkUserEmailStatus(request)
                .map(CheckEmailResponse::getStatus);
    }

    @Override
    public Single<User> socialLogin(String socialUuid, String gigSIG, String timestamp){
        SocialLoginRequest request = new SocialLoginRequest(socialUuid, gigSIG, timestamp);
        return userApi.socialLogin(request)
                .map(userInfoResponse -> {
                    UserEntity userInfoEntity = userInfoResponse.getUserInfo();
                    database.saveUserData(userInfoEntity);

                    return userInfoEntity;
                });
    }

}
