package uk.co.radicalapps.android.data.repository.datasource.network.request;


public class GetCommentsRequest {

    private String offerID;
    private String comment;
    private String nickname;
    private String email;

    public GetCommentsRequest(String offerID, String comment, String nickname, String email) {
        this.offerID = offerID;
        this.comment = comment;
        this.nickname = nickname;
        this.email = email;
    }
}
