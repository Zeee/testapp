package uk.co.radicalapps.android.data.entity;


import io.realm.RealmObject;

public class SearchSectionEntity extends RealmObject {

    public String classType;
    public String sectionName;
    public int totalResults;
    public SearchHintEntity searchHint;
}
