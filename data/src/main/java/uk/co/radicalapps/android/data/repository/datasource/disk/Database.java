package uk.co.radicalapps.android.data.repository.datasource.disk;


import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Single;
import io.realm.RealmResults;
import uk.co.radicalapps.android.data.entity.CategoryEntity;
import uk.co.radicalapps.android.data.entity.MerchantEntity;
import uk.co.radicalapps.android.data.entity.OfferEntity;
import uk.co.radicalapps.android.data.entity.PopularSearchEntity;
import uk.co.radicalapps.android.data.entity.UserEntity;

public interface Database {

    void saveAuthToken(String authToken);
    String getAuthToken();

    void saveAuthTokenSecret(String authTokenSecret);
    String getAuthTokenSecret();

    void setFirstLaunchComplete();
    boolean isFirstLaunch();

    void setUserLoggedIn();
    boolean isUserLoggedIn();

    void setUserSkippedSignUp();
    boolean hasUserSkippedSignUp();

    void saveOffer(OfferEntity offer);

    Flowable<RealmResults<OfferEntity>> getListOfOffers();

    void saveUserData(UserEntity userInfo);

    void saveCategoriesAsync(List<CategoryEntity> categories);

    void saveMerchantsAsync(List<MerchantEntity> merchantList);

    void savePopularSearchesAsync(List<PopularSearchEntity> searches);

    Single<UserEntity> getUserInfo();
}
