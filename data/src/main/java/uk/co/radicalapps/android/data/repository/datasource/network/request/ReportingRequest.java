package uk.co.radicalapps.android.data.repository.datasource.network.request;


public class ReportingRequest {

    private String offerID = null;
    private String notificationID = null;
    private String listID = null;
    private String outletID = null;
    private String source = null;
    private String timestamp = null;
    private String lat = null;
    private String lng = null;
    private String testID = null;
    private String testVersion = null;

    public ReportingRequest() {
        //do nothing
    }

    public ReportingRequest setOfferId(String offerId) {
        this.offerID = offerId;
        return this;
    }

    public ReportingRequest setNotificationId(String notificationId) {
        this.notificationID = notificationId;
        return this;
    }

    public ReportingRequest setListId(String listId) {
        this.listID = listId;
        return this;
    }

    public ReportingRequest setOutletId(String outletId) {
        this.outletID = outletId;
        return this;
    }

    public ReportingRequest setSource(String source) {
        this.source = source;
        return this;
    }

    public ReportingRequest setTimestamp(String timestamp) {
        this.timestamp = timestamp;
        return this;
    }

    public ReportingRequest setLat(String lat) {
        this.lat = lat;
        return this;
    }

    public ReportingRequest setLng(String lng) {
        this.lng = lng;
        return this;
    }

    public ReportingRequest setTestId(String testId) {
        this.testID = testId;
        return this;
    }

    public ReportingRequest setTestVersion(String testVersion) {
        this.testVersion = testVersion;
        return this;
    }
}
