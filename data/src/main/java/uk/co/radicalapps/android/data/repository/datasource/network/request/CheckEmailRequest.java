package uk.co.radicalapps.android.data.repository.datasource.network.request;


public class CheckEmailRequest {

    private String email;

    public CheckEmailRequest(String email) {
        this.email = email;
    }
}
