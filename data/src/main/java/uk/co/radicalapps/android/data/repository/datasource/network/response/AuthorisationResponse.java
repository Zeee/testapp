package uk.co.radicalapps.android.data.repository.datasource.network.response;


import uk.co.radicalapps.android.data.entity.UserEntity;

public class AuthorisationResponse {

    String oauthToken;
    String oauthTokenSecret;
    UserEntity userInfo;
    ActiveTests activeTests;

    public String getOauthToken() {
        return oauthToken;
    }

    public String getOauthTokenSecret() {
        return oauthTokenSecret;
    }

    public UserEntity getUserInfo() {
        return userInfo;
    }

    public ActiveTests getActiveTests() {
        return activeTests;
    }

    public final class ActiveTests {
        boolean zeroClick;

        public boolean isZeroClick() {
            return zeroClick;
        }
    }
}
