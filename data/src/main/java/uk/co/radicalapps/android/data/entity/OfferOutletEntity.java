package uk.co.radicalapps.android.data.entity;

import io.realm.RealmObject;

public class OfferOutletEntity extends RealmObject {

    String offerID;
    String outletID;
    String validDays;
    String offerTitle;
    String fullTermsConditions;
    String keyTermsConditions;
    String friendlyName;
    String address1;
    String address2;
    String city;
    String postcode;
    String phone;
    double longitude;
    double latitude;
    int numberOfLikes;
    double distance;

}