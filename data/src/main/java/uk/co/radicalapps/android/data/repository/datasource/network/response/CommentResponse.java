package uk.co.radicalapps.android.data.repository.datasource.network.response;


import com.google.gson.annotations.SerializedName;

import java.util.List;

import uk.co.radicalapps.android.data.entity.CommentEntity;


public class CommentResponse {

    int totalResults;
    @SerializedName("data")
    List<CommentEntity> comments;
}
