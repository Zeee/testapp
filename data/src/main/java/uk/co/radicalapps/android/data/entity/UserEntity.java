package uk.co.radicalapps.android.data.entity;


import java.util.List;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import uk.co.radicalapps.android.domain.data.User;

public class UserEntity extends RealmObject implements User {

    @PrimaryKey
    int id = 1;
    String userID;
    String firstName;
    String lastName;
    String emailAddress;
    String postCode;
    RealmList<String> favouriteMerchants;

    public int getId() {
        return id;
    }

    public String getUserID() {
        return userID;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public String getPostCode() {
        return postCode;
    }

    public RealmList<String> getFavouriteMerchants() {
        return favouriteMerchants;
    }

    public UserEntity setUserID(String userID) {
        this.userID = userID;
        return this;
    }

    public UserEntity setFirstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public UserEntity setLastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public UserEntity setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
        return this;
    }

    public UserEntity setPostCode(String postCode) {
        this.postCode = postCode;
        return this;
    }

    public UserEntity setFavouriteMerchants(List<String> favouriteMerchantIds) {
        favouriteMerchants = new RealmList<>();
        favouriteMerchants.addAll(favouriteMerchantIds);
        return this;
    }

}


