package uk.co.radicalapps.android.data.entity;


import io.realm.RealmObject;

public class SearchHintEntity extends RealmObject {
    String merchantID;
    public String query;
    public String type;
}
