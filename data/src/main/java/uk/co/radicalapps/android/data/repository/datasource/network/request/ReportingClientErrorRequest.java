package uk.co.radicalapps.android.data.repository.datasource.network.request;


public class ReportingClientErrorRequest {

    private String payload;

    public ReportingClientErrorRequest(String payload) {
        this.payload = payload;
    }
}
