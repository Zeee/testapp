package uk.co.radicalapps.android.data.repository.datasource.network.request;


public class DeviceSettingsRequest {

    private boolean bestOfferNotifications;
    private boolean geofenceNotifications;
    private String advertisingID;

    public DeviceSettingsRequest(boolean bestOfferNotifications, boolean geofenceNotifications, String advertisingID) {
        this.bestOfferNotifications = bestOfferNotifications;
        this.geofenceNotifications = geofenceNotifications;
        this.advertisingID = advertisingID;
    }
}
