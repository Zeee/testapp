package uk.co.radicalapps.android.data.entity;


import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;

public class MerchantEntity extends RealmObject {

    @PrimaryKey
    @Required
    String merchantID;
    String merchantName;
    String merchantIconURL;
    String merchantURL;


}