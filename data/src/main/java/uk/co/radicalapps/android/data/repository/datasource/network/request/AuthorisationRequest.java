package uk.co.radicalapps.android.data.repository.datasource.network.request;


public class AuthorisationRequest {

    private String token;

    public AuthorisationRequest(String token) {
        this.token = token;
    }
}
