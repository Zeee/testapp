package uk.co.radicalapps.android.data.repository.datasource.network.response;


import com.google.gson.annotations.SerializedName;

import java.util.List;

import uk.co.radicalapps.android.data.entity.OfferEntity;


public class SavedOfferResponse {

    @SerializedName("data")
    List<OfferEntity> offers;

    public List<OfferEntity> getOffers() {
        return offers;
    }
}
