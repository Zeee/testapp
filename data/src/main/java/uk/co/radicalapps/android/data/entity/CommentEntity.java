package uk.co.radicalapps.android.data.entity;


import io.realm.RealmObject;

public class CommentEntity extends RealmObject {

    String offerID;
    String comment;
    String userName;
    String dateAdded;
    boolean isSupportTeam;
    int parentOfferCommentTotal;
}
