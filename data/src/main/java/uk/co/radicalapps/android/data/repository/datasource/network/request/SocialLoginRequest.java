package uk.co.radicalapps.android.data.repository.datasource.network.request;


public class SocialLoginRequest {

    private String socialUID;
    private String gigSIG;
    private String timestamp;

    public SocialLoginRequest(String socialUId, String gigSIG, String timestamp) {
        this.socialUID = socialUId;
        this.gigSIG = gigSIG;
        this.timestamp = timestamp;
    }
}
