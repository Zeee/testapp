package uk.co.radicalapps.android.data.repository.datasource.network.response;


public class PasswordResetResponse {
    boolean success;
    String message;

    public String getMessage() {
        return message;
    }

    public boolean isSuccess() {
        return success;
    }
}
