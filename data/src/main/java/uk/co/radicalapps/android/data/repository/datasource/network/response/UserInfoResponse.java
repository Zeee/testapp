package uk.co.radicalapps.android.data.repository.datasource.network.response;


import uk.co.radicalapps.android.data.entity.UserEntity;

public class UserInfoResponse {
    UserEntity userInfo;

    public UserEntity getUserInfo() {
        return userInfo;
    }
}
