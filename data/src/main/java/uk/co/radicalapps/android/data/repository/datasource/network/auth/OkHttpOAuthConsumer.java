package uk.co.radicalapps.android.data.repository.datasource.network.auth;

import oauth.signpost.AbstractOAuthConsumer;
import oauth.signpost.http.HttpRequest;
import okhttp3.Request;

public class OkHttpOAuthConsumer extends AbstractOAuthConsumer {

    public OkHttpOAuthConsumer(String consumerKey, String consumerSecret) {
        super(consumerKey, consumerSecret);
    }

    @Override
    protected HttpRequest wrap(Object request) {
        if (request instanceof Request) {
            return new HttpRequestAdapter((Request) request);
        } else {
            throw new IllegalArgumentException("Expected OKHTTP request");
        }
    }
}
