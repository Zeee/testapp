package uk.co.radicalapps.android.data.repository.datasource.network.api;


import io.reactivex.Single;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import uk.co.radicalapps.android.data.repository.datasource.network.request.AuthorisationRequest;
import uk.co.radicalapps.android.data.repository.datasource.network.response.AuthorisationResponse;
import uk.co.radicalapps.android.data.repository.datasource.network.response.MetaDataResponse;
import uk.co.radicalapps.android.data.repository.datasource.network.response.TokenResponse;

public interface PreAuthApi {

    @GET("token/generate")
    Single<TokenResponse> generateToken();

    @POST("token/authorize")
    Single<AuthorisationResponse> authoriseToken(@Body AuthorisationRequest authorisationRequest);

    @GET("metadata/all")
    Single<MetaDataResponse> getAppMetaData();
}
