package uk.co.radicalapps.android.data.repository.datasource.network.request;


public class SaveOfferRequest {

    private String outletID;
    private String offerID;

    public SaveOfferRequest(String outletId, String offerId) {
        this.outletID = outletId;
        this.offerID = offerId;
    }
}
