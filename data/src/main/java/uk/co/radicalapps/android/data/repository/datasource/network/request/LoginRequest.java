package uk.co.radicalapps.android.data.repository.datasource.network.request;


public class LoginRequest {

    private String email;
    private String password;

    public LoginRequest(String email, String password) {
        this.email = email;
        this.password = password;
    }
}
