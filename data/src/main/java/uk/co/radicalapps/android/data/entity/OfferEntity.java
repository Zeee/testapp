package uk.co.radicalapps.android.data.entity;


import io.realm.RealmObject;

public class OfferEntity extends RealmObject {

    String offerID;
    String classType;
    boolean requiresSignIn;
    int maxUses;
    int usesByUser;
    int numberOfLikes;
    int numberOfComments;
    String merchant;
    String merchantID;
    boolean isCommentingEnabled;
    String merchantIconURL;
    String backgroundImageURL;
    String codeType;
    boolean isFeatured;
    boolean isTopLockedPlacement;
    String offerTitle;
    String code;
    boolean isFetchedCode;
    String getCodeFailAction;
    String description;
    String miscellaneous;
    String shareURL;
    String offerURL;
    String offerType;
    int startDateTime;
    int endDateTime;
    boolean isExclusive;
    OfferOutletEntity outlet;
    int totalOutlets;

    public String getOfferID() {
        return offerID;
    }

    public void setOfferID(String offerID) {
        this.offerID = offerID;
    }
}
