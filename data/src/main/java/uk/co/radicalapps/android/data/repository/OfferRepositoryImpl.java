package uk.co.radicalapps.android.data.repository;


import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Single;
import uk.co.radicalapps.android.data.entity.mapper.OfferEntityMapper;
import uk.co.radicalapps.android.data.repository.datasource.disk.Database;
import uk.co.radicalapps.android.data.repository.datasource.network.api.OfferApi;
import uk.co.radicalapps.android.data.repository.datasource.network.request.SaveOfferRequest;
import uk.co.radicalapps.android.domain.data.Offer;
import uk.co.radicalapps.android.domain.data.OfferOutlet;
import uk.co.radicalapps.android.domain.repository.OfferRepository;

public class OfferRepositoryImpl implements OfferRepository {

    private OfferApi offerApi;
    private Database database;

    public OfferRepositoryImpl(OfferApi offerApi, Database database) {
        this.offerApi = offerApi;
        this.database = database;
    }

    @Override
    public Flowable<List<Offer>> getOffers() {
        return database.getListOfOffers().map(OfferEntityMapper::transform);
    }

    @Override
    public Single<List<Offer>> getSavedOffers() {
        return offerApi.getSavedOffers()
                .map(savedOfferResponse -> OfferEntityMapper.transform(savedOfferResponse.getOffers()));
    }

    @Override
    public void saveOffer(Offer offer) {
        database.saveOffer(OfferEntityMapper.transform(offer));
    }

    @Override
    public Single<Void> saveOfferOutlet(OfferOutlet offerOutlet){
        SaveOfferRequest request = new SaveOfferRequest(offerOutlet.getOutletId(), offerOutlet.getOfferId());
        return offerApi.saveOffer(request);
    }

}
