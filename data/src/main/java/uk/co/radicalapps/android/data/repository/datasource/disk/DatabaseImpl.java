package uk.co.radicalapps.android.data.repository.datasource.disk;


import android.content.SharedPreferences;

import java.util.Collection;
import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Single;
import io.realm.Realm;
import io.realm.RealmModel;
import io.realm.RealmResults;
import uk.co.radicalapps.android.data.entity.CategoryEntity;
import uk.co.radicalapps.android.data.entity.MerchantEntity;
import uk.co.radicalapps.android.data.entity.OfferEntity;
import uk.co.radicalapps.android.data.entity.PopularSearchEntity;
import uk.co.radicalapps.android.data.entity.UserEntity;

public class DatabaseImpl implements Database {

    private SharedPreferences preferences;
    private Realm realm;

    public DatabaseImpl(SharedPreferences sharedPreferences, Realm realm) {
        this.preferences = sharedPreferences;
        this.realm = realm;
    }

    @Override
    public void saveAuthToken(String authToken){
        setString("VC_AUTH_TOKEN", authToken);
    }

    @Override
    public String getAuthToken(){
        return getString("VC_AUTH_TOKEN", null);
    }

    @Override
    public void saveAuthTokenSecret(String authTokenSecret){
        setString("VC_AUTH_TOKEN_SECRET", authTokenSecret);
    }

    @Override
    public String getAuthTokenSecret(){
        return getString("VC_AUTH_TOKEN_SECRET", null);
    }

    @Override
    public void setFirstLaunchComplete() {
        setBoolean("FIRST_LAUNCH", false);
    }

    @Override
    public boolean isFirstLaunch() {
        return getBoolean("FIRST_LAUNCH", true);
    }

    @Override
    public void setUserLoggedIn() {
        setBoolean("USER_LOGGED_IN", true);
    }

    @Override
    public boolean isUserLoggedIn() {
        return getBoolean("USER_LOGGED_IN", false);
    }

    @Override
    public void setUserSkippedSignUp() {
        setBoolean("SIGN_UP_SKIPPED", true);
    }

    @Override
    public boolean hasUserSkippedSignUp() {
        return getBoolean("SIGN_UP_SKIPPED", false);
    }

    @Override
    public void saveOffer(OfferEntity offerEntity) {
        insertOrUpdateItemAsync(offerEntity);
    }

    @Override
    public Flowable<RealmResults<OfferEntity>> getListOfOffers() {
        return realm.where(OfferEntity.class).findAllAsync()
                .asFlowable()
                .filter(RealmResults::isLoaded);
    }

    @Override
    public void saveUserData(UserEntity userInfo) {
        insertOrUpdateItemAsync(userInfo);
    }

    @Override
    public void saveCategoriesAsync(List<CategoryEntity> categories) {
        insertOrUpdateItemsAsync(categories);
    }

    @Override
    public void saveMerchantsAsync(List<MerchantEntity> merchantList) {
        insertOrUpdateItemsAsync(merchantList);
    }

    @Override
    public void savePopularSearchesAsync(List<PopularSearchEntity> searches) {
        insertOrUpdateItemsAsync(searches);
    }

    @Override
    public Single<UserEntity> getUserInfo() {
        return null;
    }




    private void insertOrUpdateItemsAsync(Collection<? extends RealmModel> list){
        try(Realm realm = Realm.getDefaultInstance()){
            realm.executeTransaction(realmInstance -> realmInstance.insertOrUpdate(list));
        }
    }

    private void insertOrUpdateItemAsync(RealmModel realmModel){
        try(Realm realm = Realm.getDefaultInstance()){
            realm.executeTransaction(realmInstance -> realmInstance.insertOrUpdate(realmModel));
        }
    }

    /**
     * Helper methods for storing/retrieving preferences data
     */

    private SharedPreferences.Editor edit(){
        return preferences.edit();
    }

    private void setString(String key, String value){
        edit().putString(key, value).commit();
    }

    private String getString(String key, String defValue){
        return preferences.getString(key, defValue);
    }

    private void setBoolean(String key, boolean value){
        edit().putBoolean(key, value).commit();
    }

    private boolean getBoolean(String key, boolean defValue){
        return preferences.getBoolean(key, defValue);
    }
}
