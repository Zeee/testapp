package uk.co.radicalapps.android.data.entity.mapper;


import io.realm.RealmList;
import uk.co.radicalapps.android.data.entity.UserEntity;
import uk.co.radicalapps.android.domain.data.User;

public class UserEntityMapper {

    public static UserEntity transform(User user){
        RealmList<String> favMerchants = new RealmList<>();
        favMerchants.addAll(user.getFavouriteMerchants());

        return new UserEntity()
                .setUserID(user.getUserID())
                .setEmailAddress(user.getEmailAddress())
                .setFirstName(user.getFirstName())
                .setLastName(user.getLastName())
                .setPostCode(user.getPostCode())
                .setFavouriteMerchants(favMerchants);
    }
}
