package uk.co.radicalapps.android.data.repository.datasource.network.api;


import org.json.JSONObject;

import java.util.List;

import io.reactivex.Single;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;
import uk.co.radicalapps.android.data.entity.OfferEntity;
import uk.co.radicalapps.android.data.repository.datasource.network.request.GeoFenceLogRequest;
import uk.co.radicalapps.android.data.repository.datasource.network.request.GeoFenceRequest;
import uk.co.radicalapps.android.data.repository.datasource.network.request.GetCommentsRequest;
import uk.co.radicalapps.android.data.repository.datasource.network.request.MerchantRequest;
import uk.co.radicalapps.android.data.repository.datasource.network.request.OfferLikeRequest;
import uk.co.radicalapps.android.data.repository.datasource.network.request.RateOfferRequest;
import uk.co.radicalapps.android.data.repository.datasource.network.request.SaveOfferRequest;
import uk.co.radicalapps.android.data.repository.datasource.network.response.CommentResponse;
import uk.co.radicalapps.android.data.repository.datasource.network.response.GeocodeLookupResponse;
import uk.co.radicalapps.android.data.repository.datasource.network.response.OfferCodeResponse;
import uk.co.radicalapps.android.data.repository.datasource.network.response.OfferListResponse;
import uk.co.radicalapps.android.data.repository.datasource.network.response.OfferOutletListResponse;
import uk.co.radicalapps.android.data.repository.datasource.network.response.OutclickResponse;
import uk.co.radicalapps.android.data.repository.datasource.network.response.SavedOfferResponse;

public interface OfferApi {

    @GET("offer/getlist")
    Single<OfferListResponse> getList(@Query("list") String list, @Query("listID") String listId, @Query("lat") String latitude, @Query("lng") String longitude, @Query("radius") int radius, @Query("offset") int offsetKey, @Query("limit") int limit, @Query("testID") String testId, @Query("testVersion") String testVersion);

    @GET("offer/getoutlets")
    Single<OfferOutletListResponse> getStoresWhereOfferCanBeUsed(@Query("offerID") String offerId, @Query("lat") String latitude, @Query("lng") String longitude, @Query("offset") int offsetKey, @Query("limit") int limit);

    @GET("offer/getoutclick")
    Single<OutclickResponse> getOutClick(@Query("offerID") String offerId, @Query("sessionID") String sessionId);

    @GET("offer/get")
    Single<OfferEntity> getOffer(@Query("offerID") String offerId, @Query("lat") String latitude, @Query("lng") String longitude);

    @GET("offer/getcode")
    Single<OfferCodeResponse> redeemCode(@Query("offerID") String offerId);

    @POST("offer/like")
    Single<Void> likeOffer(@Body OfferLikeRequest offerLikeRequest);

    @POST("offer/rate")
    Single<Void> rateOffer(@Body RateOfferRequest rateOfferRequest);

    @POST("user/savedoffers")
    Single<Void> saveOffer(@Body SaveOfferRequest saveOfferRequest);

    @GET("user/savedoffers")
    Single<SavedOfferResponse> getSavedOffers();

    @DELETE("user/savedoffers/{offerID}")
    Single<Void> removeSavedOffer(@Path("offerID") String offerId);

    @GET("geocode/lookup")
    Single<GeocodeLookupResponse> getGeocode(@Query("type") String type, @Query("query") String query);

    @GET("search/all")
    Single<List<JSONObject>> search(@Query("searchHint") String searchHint, @Query("lat") String latitude, @Query("lng") String longitude, @Query("testID") int testId, @Query("testVersion") int testVersion);

    @GET("offer/getcomments")
    Single<CommentResponse> getComments(@Query("offerID") String offerId, @Query("offset") int offsetKey, @Query("limit") int limit);

    @POST("offer/getcomments")
    Single<Void> getComments(@Body GetCommentsRequest getCommentsRequest);

    @POST("geofence/enter")
    Single<Void> enterGeofence(@Body GeoFenceRequest geoFenceRequest);

    @POST("geofence/leave")
    Single<Void> leaveGeofence(@Body GeoFenceRequest geoFenceRequest);

    @POST("geofence/log")
    Single<Void> logGeofence(@Body GeoFenceLogRequest geoFenceLogRequest);

    @POST("user/favoritedmerchants")
    Single<Void> saveFavouriteMerchant(@Body MerchantRequest merchantRequest);

    @POST("user/favoritedmerchants")
    Single<Void> putFavouriteMerchant(@Body MerchantRequest merchantRequest);

    @DELETE("user/favoritedmerchants")
    Single<Void> removeFavouriteMerchant(@Body MerchantRequest merchantRequest);




}
