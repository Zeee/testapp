package uk.co.radicalapps.android.data.repository.datasource.network.request;


public class MerchantRequest {

    private String merchantID;

    public MerchantRequest(String merchantId) {
        this.merchantID = merchantId;
    }
}
