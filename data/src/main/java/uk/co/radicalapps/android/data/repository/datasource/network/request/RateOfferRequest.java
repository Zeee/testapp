package uk.co.radicalapps.android.data.repository.datasource.network.request;


public class RateOfferRequest {

    private String offerID;
    private int rating;
    private String comment;

    public RateOfferRequest(String offerID, int rating, String comment) {
        this.offerID = offerID;
        this.rating = rating;
        this.comment = comment;
    }
}
