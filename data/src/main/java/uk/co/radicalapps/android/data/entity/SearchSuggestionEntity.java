package uk.co.radicalapps.android.data.entity;


import io.realm.RealmObject;

public class SearchSuggestionEntity extends RealmObject {
    String classType;
    String title;
    String iconURL;

    public static class SearchHint {
        String merchantID;
    }
}
