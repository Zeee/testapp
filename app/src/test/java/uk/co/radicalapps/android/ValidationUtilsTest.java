package uk.co.radicalapps.android;


import org.junit.Before;
import org.junit.Test;

import uk.co.radicas.android.util.ValidationUtils;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class ValidationUtilsTest {


    @Before
    public void setUp(){
        new ValidationUtils();
    }

    @Test
    public void testValidEmails(){
        boolean isValid = ValidationUtils.isEmailValid("test@test.com");
        assertTrue(isValid);
        isValid = ValidationUtils.isEmailValid("test1@test.com");
        assertTrue(isValid);
        isValid = ValidationUtils.isEmailValid("test@test1.com");
        assertTrue(isValid);
        isValid = ValidationUtils.isEmailValid("test.test@test.com");
        assertTrue(isValid);
        isValid = ValidationUtils.isEmailValid("Test@test.com");
        assertTrue(isValid);
        isValid = ValidationUtils.isEmailValid("test-test@test.com");
        assertTrue(isValid);
        isValid = ValidationUtils.isEmailValid("test+test@test.com");
        assertTrue(isValid);
        isValid = ValidationUtils.isEmailValid("test_test@test.com");
        assertTrue(isValid);
    }


    @Test
    public void testInvalidEmails(){
        boolean isValid = ValidationUtils.isEmailValid("");
        assertFalse(isValid);
        isValid = ValidationUtils.isEmailValid("test@.c");
        assertFalse(isValid);
        isValid = ValidationUtils.isEmailValid("test@1com");
        assertFalse(isValid);
        isValid = ValidationUtils.isEmailValid("testtest1.com");
        assertFalse(isValid);
        isValid = ValidationUtils.isEmailValid("test@test1.co.");
        assertFalse(isValid);
        isValid = ValidationUtils.isEmailValid("test@test1.");
        assertFalse(isValid);
        isValid = ValidationUtils.isEmailValid("@test1.com");
        assertFalse(isValid);

    }
}
