package uk.co.radicalapps.android;

import com.google.gson.Gson;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import io.reactivex.Single;
import io.reactivex.schedulers.TestScheduler;
import io.reactivex.subjects.PublishSubject;
import uk.co.radicas.android.repo.IRepository;
import uk.co.radicas.android.repo.network.response.UserInfoResponse;
import uk.co.radicas.android.ui.onboarding.register.RegisterPresenter;
import uk.co.radicalapps.android.util.MockVars;

import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static uk.co.radicalapps.android.util.MockJsonResponses.USER_INFO_RESPONSE;
import static uk.co.radicalapps.android.util.MockVars.EMPTY_STRING;
import static uk.co.radicalapps.android.util.MockVars.INVALID_PASSWORD;
import static uk.co.radicalapps.android.util.MockVars.VALID_EMAIL;
import static uk.co.radicalapps.android.util.MockVars.VALID_PASSWORD;

public class RegisterPresenterTest {

    private PublishSubject<String> emailPubSub = PublishSubject.create();
    private PublishSubject<String> passwordPubSub = PublishSubject.create();
    private PublishSubject<Object> registerPubSub = PublishSubject.create();

    @Mock private IRepository repository;
    @Mock private RegisterPresenter.View view;

    private RegisterPresenter presenter;
    private TestScheduler testScheduler;
    private Gson gson;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        testScheduler = new TestScheduler();
        gson = new Gson();

        when(view.getOnEmailTextChangeObs()).thenReturn(emailPubSub);
        when(view.getOnPasswordTextChangeObs()).thenReturn(passwordPubSub);
        when(view.getOnRegisterClickObs()).thenReturn(registerPubSub);

        presenter = new RegisterPresenter(repository, testScheduler, testScheduler);
        presenter.attachView(view);
    }

    @After
    public void tearDown(){
        presenter.detachView();
        presenter = null;
    }

    @Test
    public void testThatViewIsObservedOnAttach() throws Exception {
        verify(view, times(1)).getOnEmailTextChangeObs();
        verify(view, times(1)).getOnPasswordTextChangeObs();
        verify(view, times(1)).getOnRegisterClickObs();
        verifyNoMoreInteractions(view);
    }

    @Test
    public void testThatWhenEmailIncorrectErrorIsShown() throws Exception {
        passwordPubSub.onNext(EMPTY_STRING);
        emailPubSub.onNext(EMPTY_STRING);

        testScheduler.advanceTimeBy(500, MILLISECONDS);

        emailPubSub.onNext(MockVars.INVALID_EMAIL);

        testScheduler.advanceTimeBy(500, MILLISECONDS);

        verify(view, times(1)).showEmailError(true);
        verify(view, times(2)).enableRegistrationButton(false);
    }

    @Test
    public void testThatWhenEmailCorrectNoErrorIsShown() throws Exception {
        passwordPubSub.onNext(EMPTY_STRING);
        emailPubSub.onNext(EMPTY_STRING);

        testScheduler.advanceTimeBy(500, MILLISECONDS);

        emailPubSub.onNext(VALID_EMAIL);

        testScheduler.advanceTimeBy(500, MILLISECONDS);

        verify(view, times(1)).showEmailError(false);
        verify(view, times(2)).enableRegistrationButton(false);
    }

    @Test
    public void testThatWhenPasswordIncorrectErrorIsShown() throws Exception {
        emailPubSub.onNext(EMPTY_STRING);
        passwordPubSub.onNext(EMPTY_STRING);

        testScheduler.advanceTimeBy(500, MILLISECONDS);

        passwordPubSub.onNext(INVALID_PASSWORD);

        testScheduler.advanceTimeBy(500, MILLISECONDS);

        verify(view, times(1)).showPasswordError(true);
        verify(view, times(2)).enableRegistrationButton(false);
    }

    @Test
    public void testThatWhenPasswordCorrectNoErrorIsShown() throws Exception {
        emailPubSub.onNext(EMPTY_STRING);
        passwordPubSub.onNext(EMPTY_STRING);

        testScheduler.advanceTimeBy(500, MILLISECONDS);

        passwordPubSub.onNext(VALID_PASSWORD);

        testScheduler.advanceTimeBy(500, MILLISECONDS);

        verify(view, times(1)).showPasswordError(false);
        verify(view, times(2)).enableRegistrationButton(false);
    }

    @Test
    public void testThatWhenEmailAndPasswordCorrectNoErrorIsShownAndButtonIsEnabled() throws Exception {
        emailPubSub.onNext(EMPTY_STRING);
        passwordPubSub.onNext(EMPTY_STRING);

        testScheduler.advanceTimeBy(500, MILLISECONDS);

        emailPubSub.onNext(VALID_EMAIL);
        passwordPubSub.onNext(VALID_PASSWORD);

        testScheduler.advanceTimeBy(500, MILLISECONDS);

        verify(view, times(1)).showEmailError(false);
        verify(view, times(1)).showPasswordError(false);
        verify(view, times(1)).enableRegistrationButton(true);
    }

    @Test
    public void testThatWhenEmailAndPasswordIncorrectErrorIsShownAndButtonIsDisabled() throws Exception {
        emailPubSub.onNext(EMPTY_STRING);
        passwordPubSub.onNext(EMPTY_STRING);

        testScheduler.advanceTimeBy(500, MILLISECONDS);

        emailPubSub.onNext(MockVars.INVALID_EMAIL);
        passwordPubSub.onNext(INVALID_PASSWORD);

        testScheduler.advanceTimeBy(500, MILLISECONDS);

        verify(view, times(1)).showEmailError(true);
        verify(view, times(1)).showPasswordError(true);
        verify(view, times(3)).enableRegistrationButton(false);
    }

    @Test
    public void testThatWhenRegisterIsClickedWithSuccessfulResponseMoveToNextScreen(){
        UserInfoResponse userInfoResponse = gson.fromJson(USER_INFO_RESPONSE, UserInfoResponse.class);

        when(repository.preAuthDevice()).thenReturn(Single.just(true));
        when(repository.registerNewUser(VALID_EMAIL, VALID_PASSWORD))
                .thenReturn(Single.just(userInfoResponse.getUserInfo()));

        emailPubSub.onNext(EMPTY_STRING);
        passwordPubSub.onNext(EMPTY_STRING);

        testScheduler.advanceTimeBy(500, MILLISECONDS);

        emailPubSub.onNext(VALID_EMAIL);
        passwordPubSub.onNext(VALID_PASSWORD);

        testScheduler.advanceTimeBy(500, MILLISECONDS);

        registerPubSub.onNext(new Object());

        testScheduler.triggerActions();

        verify(view, never()).showEmailError(true);
        verify(view, never()).showPasswordError(true);
        verify(view, times(1)).showEmailError(false);
        verify(view, times(1)).showPasswordError(false);
        verify(view, times(1)).enableRegistrationButton(true);
        verify(view).showLoading(true);
        verify(view).showLoading(false);
        verify(view).goToMainActivity();
    }


    @Test
    public void testThatWhenRegisterIsClickedWithFailedPreAuthShowError(){
        UserInfoResponse userInfoResponse = gson.fromJson(USER_INFO_RESPONSE, UserInfoResponse.class);

        when(repository.preAuthDevice()).thenReturn(Single.error(new Exception("Something went wrong")));
        when(repository.registerNewUser(VALID_EMAIL, VALID_PASSWORD))
                .thenReturn(Single.just(userInfoResponse.getUserInfo()));

        emailPubSub.onNext(EMPTY_STRING);
        passwordPubSub.onNext(EMPTY_STRING);

        testScheduler.advanceTimeBy(500, MILLISECONDS);

        emailPubSub.onNext(VALID_EMAIL);
        passwordPubSub.onNext(VALID_PASSWORD);

        testScheduler.advanceTimeBy(500, MILLISECONDS);

        registerPubSub.onNext(new Object());

        testScheduler.triggerActions();

        verify(view, never()).showEmailError(true);
        verify(view, never()).showPasswordError(true);
        verify(view, times(1)).showEmailError(false);
        verify(view, times(1)).showPasswordError(false);
        verify(view, times(1)).enableRegistrationButton(true);
        verify(view).showLoading(true);
        verify(view).showRegistrationError("Something went wrong");
        verify(view).showLoading(false);
    }

    @Test
    public void testThatWhenRegisterIsClickedWithFailedRegShowError(){
        when(repository.preAuthDevice()).thenReturn(Single.just(true));
        when(repository.registerNewUser(MockVars.VALID_EMAIL, VALID_PASSWORD))
                .thenReturn(Single.error(new Exception()));

        emailPubSub.onNext(EMPTY_STRING);
        passwordPubSub.onNext(EMPTY_STRING);

        testScheduler.advanceTimeBy(500, MILLISECONDS);

        emailPubSub.onNext(MockVars.VALID_EMAIL);
        passwordPubSub.onNext(VALID_PASSWORD);

        testScheduler.advanceTimeBy(500, MILLISECONDS);

        registerPubSub.onNext(new Object());

        testScheduler.triggerActions();

        verify(view, never()).showEmailError(true);
        verify(view, never()).showPasswordError(true);
        verify(view, times(1)).showEmailError(false);
        verify(view, times(1)).showPasswordError(false);
        verify(view, times(1)).enableRegistrationButton(true);
        verify(view).showLoading(true);
        verify(view).showRegistrationError("Something went wrong");
        verify(view).showLoading(false);
    }


}