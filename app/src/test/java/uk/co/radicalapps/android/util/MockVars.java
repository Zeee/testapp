package uk.co.radicalapps.android.util;


public interface MockVars {

    String VALID_EMAIL = "test@gmail.com";
    String INVALID_EMAIL = "test@gmailcom";
    String VALID_PASSWORD = "password";
    String INVALID_PASSWORD = "pass";
    String EMPTY_STRING = "";
}
