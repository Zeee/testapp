package uk.co.radicalapps.android;


import com.google.gson.Gson;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subscribers.TestSubscriber;
import uk.co.radicas.android.repo.Repository;
import uk.co.radicas.android.repo.data.IDataStore;
import uk.co.radicas.android.repo.network.api.DeviceApi;
import uk.co.radicas.android.repo.network.api.OfferApi;
import uk.co.radicas.android.repo.network.api.PreAuthApi;
import uk.co.radicas.android.repo.network.api.ReportingApi;
import uk.co.radicas.android.repo.network.api.UserApi;
import uk.co.radicas.android.repo.network.response.AuthorisationResponse;
import uk.co.radicas.android.repo.network.response.MetaDataResponse;
import uk.co.radicas.android.repo.network.response.TokenResponse;
import uk.co.radicas.android.repo.network.response.UserInfoResponse;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.when;
import static uk.co.radicalapps.android.util.MockJsonResponses.AUTH_RESPONSE;
import static uk.co.radicalapps.android.util.MockJsonResponses.EMPTY_JSON;
import static uk.co.radicalapps.android.util.MockJsonResponses.META_DATA_RESPONSE;
import static uk.co.radicalapps.android.util.MockJsonResponses.SIGN_UP_RESPONSE;
import static uk.co.radicalapps.android.util.MockJsonResponses.TOKEN_RESPONSE;

public class RepositoryTest {
    @Mock
    private PreAuthApi preAuthApi;
    @Mock
    private UserApi userApi;
    @Mock
    private OfferApi offerApi;
    @Mock
    private DeviceApi deviceApi;
    @Mock
    private ReportingApi reportingApi;
    @Mock
    private IDataStore dataStore;
    private Gson gson;

    private Repository repository; //Class under test


    @Before
    public void setUp(){
        MockitoAnnotations.initMocks(this);
        repository = new Repository(preAuthApi, deviceApi, offerApi, reportingApi, userApi, dataStore);
        gson = new Gson();
    }

    @After
    public void tearDown(){
        repository = null;
    }

    @Test
    public void testPreAuthSuccessful(){
        TestSubscriber<Boolean> subscriber = new TestSubscriber<>();
        when(preAuthApi.generateToken()).thenReturn(Single.just(gson.fromJson(TOKEN_RESPONSE, TokenResponse.class)));
        when(preAuthApi.authoriseToken(anyObject())).thenReturn(Single.just(gson.fromJson(AUTH_RESPONSE, AuthorisationResponse.class)));
        when(preAuthApi.getAppMetaData()).thenReturn(Single.just(gson.fromJson(META_DATA_RESPONSE, MetaDataResponse.class)));

        repository.preAuthDevice().subscribeOn(Schedulers.trampoline()).toFlowable().subscribe(subscriber);

        subscriber.assertSubscribed();
        subscriber.assertNoErrors();
        subscriber.assertValue(true);
        subscriber.assertNever(false);
        subscriber.assertComplete();
    }

    @Test
    public void testGenTokenEmptyResponseThrowsException(){
        TestSubscriber<Object> subscriber = new TestSubscriber<>();
        when(preAuthApi.generateToken()).thenReturn(Single.just(gson.fromJson(EMPTY_JSON, TokenResponse.class)));

        repository.preAuthDevice().subscribeOn(Schedulers.trampoline()).toFlowable().subscribe(subscriber);

        subscriber.assertSubscribed();
        subscriber.assertError(NullPointerException.class);
        subscriber.assertErrorMessage("Auth token is null");
        subscriber.assertNotComplete();
        subscriber.assertTerminated();
    }

    @Test
    public void testAuthTokenEmptyResponseThrowsException(){
        TestSubscriber<Object> subscriber = new TestSubscriber<>();
        when(preAuthApi.generateToken()).thenReturn(Single.just(gson.fromJson(TOKEN_RESPONSE, TokenResponse.class)));
        when(preAuthApi.authoriseToken(anyObject())).thenReturn(Single.just(gson.fromJson(EMPTY_JSON, AuthorisationResponse.class)));

        repository.preAuthDevice().subscribeOn(Schedulers.trampoline()).toFlowable().subscribe(subscriber);

        subscriber.assertSubscribed();
        subscriber.assertError(NullPointerException.class);
        subscriber.assertErrorMessage("Client authentication failed");
        subscriber.assertNotComplete();
        subscriber.assertTerminated();
    }

    @Test
    public void testMetaDataEmptyResponseThrowsException(){
        TestSubscriber<Object> subscriber = new TestSubscriber<>();
        when(preAuthApi.getAppMetaData()).thenReturn(Single.just(gson.fromJson(EMPTY_JSON, MetaDataResponse.class)));

        repository.fetchMetadata().subscribeOn(Schedulers.trampoline()).toFlowable().subscribe(subscriber);

        subscriber.assertSubscribed();
        subscriber.assertError(NullPointerException.class);
        subscriber.assertErrorMessage("Incomplete meta-data was returned");
        subscriber.assertNotComplete();
        subscriber.assertTerminated();
    }

    @Test
    public void testRegisterUserSuccess(){
        TestSubscriber<Object> subscriber = new TestSubscriber<>();
        UserInfoResponse userInfoResponse = gson.fromJson(SIGN_UP_RESPONSE, UserInfoResponse.class);
        when(userApi.signUp(anyObject())).thenReturn(Single.just(userInfoResponse));

        repository.registerNewUser("email", "password").subscribeOn(Schedulers.trampoline()).toFlowable().subscribe(subscriber);

        subscriber.assertSubscribed();
        subscriber.assertNoErrors();
        subscriber.assertValue(userInfoResponse.getUserInfo());
        subscriber.assertComplete();
    }

    @Test
    public void testRegisterUserEmptyResponse(){
        TestSubscriber<Object> subscriber = new TestSubscriber<>();
        when(userApi.signUp(anyObject())).thenReturn(Single.just(gson.fromJson(EMPTY_JSON, UserInfoResponse.class)));

        repository.registerNewUser("email", "password").subscribeOn(Schedulers.trampoline()).toFlowable().subscribe(subscriber);

        subscriber.assertSubscribed();
        subscriber.assertNoValues();
        subscriber.assertNotComplete();
        subscriber.assertError(NullPointerException.class);
        subscriber.assertErrorMessage("Received invalid data from backend");
    }



    @Test
    public void testUserIsLoggedIn(){
        when(dataStore.isUserLoggedIn()).thenReturn(true);

        boolean value = repository.isUserLoggedIn();

        assertEquals(true, value);
    }

    @Test
    public void testHasSkippedSignUp(){
        when(dataStore.hasUserSkippedSignUp()).thenReturn(true);

        boolean value = repository.hasUserSkippedSignUp();

        assertEquals(true, value);
    }
}
