package uk.co.radicalapps.android;


import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import io.reactivex.Scheduler;
import uk.co.radicas.android.ui.base.BasePresenter;
import uk.co.radicas.android.ui.base.BaseView;

import static org.mockito.Mockito.verifyZeroInteractions;

public class BasePresenterTest {

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Mock
    Scheduler uiScheduler;
    @Mock
    Scheduler ioScheduler;
    @Mock
    BaseView view;

    BasePresenter<BaseView> presenter;


    @Before
    public void setUp(){
        MockitoAnnotations.initMocks(this);
        presenter = new BasePresenter<>(ioScheduler, uiScheduler);
    }

    @After
    public void tearDown(){
        presenter = null;
    }

    @Test
    public void testViewAttach(){
        presenter.attachView(view);

        verifyZeroInteractions(view);
    }

    @Test
    public void testViewAttachWithNullView(){
        thrown.expect(NullPointerException.class);
        thrown.expectMessage("View cannot be null");

        presenter.attachView(null);

        verifyZeroInteractions(view);
    }

    @Test
    public void testDetachView(){
        presenter.attachView(view);
        presenter.detachView();

        verifyZeroInteractions(view);
    }

    @Test
    public void testDetachViewBeforeCallingAttach(){
        thrown.expect(IllegalStateException.class);
        thrown.expectMessage("Cannot call detachView() before attachView(view)");

        presenter.detachView();

        verifyZeroInteractions(view);
    }

    @Test
    public void testIsViewAttachedWhenItIs(){
        presenter.attachView(view);
        presenter.checkViewAttached();

        verifyZeroInteractions(view);
    }

    @Test
    public void testIsViewAttachedWhenItIsNot(){
        thrown.expect(BasePresenter.MvpViewNotAttachedException.class);
        thrown.expectMessage("Please call Presenter.attachView(MvpView) before requesting data to the Presenter");

        presenter.checkViewAttached();

        verifyZeroInteractions(view);
    }


}
