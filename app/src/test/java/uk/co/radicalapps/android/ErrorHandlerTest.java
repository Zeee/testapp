package uk.co.radicalapps.android;


import org.junit.Before;
import org.junit.Test;

import okhttp3.MediaType;
import okhttp3.ResponseBody;
import retrofit2.HttpException;
import retrofit2.Response;
import uk.co.radicas.android.repo.network.response.UserInfoResponse;
import uk.co.radicas.android.util.ErrorHandler;

import static org.junit.Assert.assertEquals;

public class ErrorHandlerTest {

    @Before
    public void setUp(){
        ErrorHandler errorHandler = new ErrorHandler();
    }

    @Test
    public void testHttpException(){
        ResponseBody responseBody = ResponseBody.create(MediaType.parse("text/plain"), "");
        Response<UserInfoResponse> response = Response.error(400, responseBody);
        String message = ErrorHandler.getErrorMessage(new HttpException(response));

        assertEquals("Response.error()", message);
    }

    @Test
    public void testIllegalArgException(){
        String message = ErrorHandler.getErrorMessage(new IllegalArgumentException("Something illegal"));

        assertEquals("User input error", message);
    }

    @Test
    public void testException(){
        String message = ErrorHandler.getErrorMessage(new Exception("Something illegal"));

        assertEquals("Something went wrong", message);
    }

    @Test
    public void testUnspecifiedException(){
        String message = ErrorHandler.getErrorMessage(new ArrayIndexOutOfBoundsException("Something illegal"));

        assertEquals("Something went wrong", message);
    }
}
