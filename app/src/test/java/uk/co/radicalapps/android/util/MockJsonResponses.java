package uk.co.radicalapps.android.util;


public interface MockJsonResponses {

    String EMPTY_JSON = "{}";

    String TOKEN_RESPONSE =
            "{\n" +
            "    \"token\": \"3f3c9b4d2442812c5b29a09f3cec23c2\"\n" +
            "}";

    String AUTH_RESPONSE =
                    "{\n" +
                    "    \"oauthToken\": \"d7872565c32b20a2cc476c10a43b785b\",\n" +
                    "    \"oauthTokenSecret\": \"ca1194ea27304d43f1cea5bf7f52f6b94c32a571f0cd11b547f9123a76aa2e9c\",\n" +
                    "    \"userInfo\": {\n" +
                    "        \"userID\": null,\n" +
                    "        \"firstName\": null,\n" +
                    "        \"lastName\": null,\n" +
                    "        \"postCode\": null,\n" +
                    "        \"emailAddress\": null,\n" +
                    "        \"favoriteMerchants\": []\n" +
                    "    },\n" +
                    "    \"activeTests\": {\n" +
                    "        \"zeroClick\": true\n" +
                    "    }\n" +
                    "}";

    String META_DATA_RESPONSE =
            "{\n" +
                    "    \"InstoreCategory\": [\n" +
                    "        {\n" +
                    "            \"categoryID\": \"vc-category-269\",\n" +
                    "            \"categoryName\": \"Restaurants\"\n" +
                    "        }" +
                    "    ],\n" +
                    "    \"OnlineCategory\": [\n" +
                    "        {\n" +
                    "            \"categoryID\": \"vc-category-2\",\n" +
                    "            \"categoryName\": \"Womens Fashion\"\n" +
                    "        }" +
                    "    ],\n" +
                    "    \"PopularSearches\": [\n" +
                    "        {\n" +
                    "            \"searchName\": \"Clarks\",\n" +
                    "            \"searchHint\": {\n" +
                    "                \"merchantID\": \"vc-merchant-1329\"\n" +
                    "            }\n" +
                    "        }" +
                    "    ],\n" +
                    "    \"PopularMerchants\": [\n" +
                    "        {\n" +
                    "            \"merchantID\": \"vc-merchant-3000\",\n" +
                    "            \"merchantName\": \"Accessorize\",\n" +
                    "            \"merchantIconURL\": \"http://static.vouchercodes.co.uk/mobile/icons/3000_130903114936.png\",\n" +
                    "            \"merchantURL\": \"accessorize.com\"\n" +
                    "        }" +
                    "    ]\n" +
                    "}";

    String USER_INFO_RESPONSE =
                    "{\n" +
                    "   \"userID\": 26548100,\n" +
                    "   \"userInfo\": {\n" +
                    "      \"userID\": \"26548100\",\n" +
                    "      \"firstName\": null,\n" +
                    "      \"lastName\": null,\n" +
                    "      \"postCode\": null,\n" +
                    "      \"emailAddress\": \"gerger@cvv.com\",\n" +
                    "      \"favoriteMerchants\": []\n" +
                    "   }\n" +
                    "}";

    String SIGN_UP_RESPONSE =
                    "{\n" +
                    "    \"userID\": 26602066,\n" +
                    "    \"userInfo\": {\n" +
                    "        \"userID\": \"26602066\",\n" +
                    "        \"firstName\": \"MyName\",\n" +
                    "        \"lastName\": \"Hwllo\",\n" +
                    "        \"postCode\": \"wx1d 8xz\",\n" +
                    "        \"emailAddress\": \"random12e1233@rmn.com\",\n" +
                    "        \"favoriteMerchants\": []\n" +
                    "    }\n" +
                    "}";


}
