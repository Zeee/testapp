package uk.co.radicalapps.android;

import android.support.test.filters.LargeTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import uk.co.radicas.android.ui.onboarding.OnboardingActivity;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDescendantOfA;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;
import static uk.co.radicalapps.android.Utils.childOf;

@RunWith(AndroidJUnit4.class)
@LargeTest
public class OnboardingActivityTest {

    @Rule
    public ActivityTestRule<OnboardingActivity> mActivityRule = new ActivityTestRule<>(OnboardingActivity.class);

    @Test
    public void listGoesOverTheFold() {
        onView(
                allOf(withText("this is info"),
                        isDescendantOfA(childOf(1, withId(R.id.onboarding_viewpager))))
        )
                .check(matches(isDisplayed()));
    }

}
