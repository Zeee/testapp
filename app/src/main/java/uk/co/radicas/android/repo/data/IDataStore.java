package uk.co.radicas.android.repo.data;


import java.util.List;

import uk.co.radicas.android.repo.data.model.Merchant;
import uk.co.radicas.android.repo.data.model.UserInfo;
import uk.co.radicas.android.repo.data.model.Category;
import uk.co.radicas.android.repo.data.model.Offer;
import uk.co.radicas.android.repo.data.model.PopularSearch;

public interface IDataStore {

    void saveAuthToken(String authToken);
    String getAuthToken();

    void saveAuthTokenSecret(String authTokenSecret);
    String getAuthTokenSecret();

    void setFirstLaunchComplete();
    boolean isFirstLaunch();

    void setUserLoggedIn();
    boolean isUserLoggedIn();

    void setUserSkippedSignUp();
    boolean hasUserSkippedSignUp();



    List<Offer> getListOfOffers();

    void saveUserData(UserInfo userInfo);

    void saveCategories(List<Category> categories);

    void saveMerchants(List<Merchant> merchantList);

    void savePopularSearches(List<PopularSearch> searches);
}
