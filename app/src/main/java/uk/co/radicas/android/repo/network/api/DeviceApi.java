package uk.co.radicas.android.repo.network.api;


import io.reactivex.Single;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import uk.co.radicas.android.repo.network.request.DeviceSettingsRequest;
import uk.co.radicas.android.repo.network.request.GetTermsRequest;
import uk.co.radicas.android.repo.network.request.NotificationRequest;
import uk.co.radicas.android.repo.network.response.DeviceSettingsResponse;
import uk.co.radicas.android.repo.network.response.TermsResponse;

public interface DeviceApi {

    @GET("device/settings")
    Single<DeviceSettingsResponse> getDeviceSettings();

    @POST("device/settings")
    Single<Void> sendDeviceSettings(@Body DeviceSettingsRequest deviceSettingsRequest);

    @GET("device/terms")
    Single<TermsResponse> getTermsVersion();

    @POST("device/terms")
    Single<Void> getTermsVersion(@Body GetTermsRequest getTermsRequest);

    @POST("notifications/registration")
    Single<Void> registerPushToken(@Body NotificationRequest notificationRequest);

    @DELETE("notifications/registration")
    Single<Void> unregisterPushToken(@Body NotificationRequest notificationRequest);

}
