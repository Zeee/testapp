package uk.co.radicas.android.repo.data.model;


import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;

public class Category extends RealmObject {

    @PrimaryKey
    @Required
    private String categoryID;
    private String categoryName;


}
