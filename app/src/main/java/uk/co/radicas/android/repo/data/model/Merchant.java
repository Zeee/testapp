package uk.co.radicas.android.repo.data.model;


import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;

public class Merchant extends RealmObject {

    @PrimaryKey
    @Required
    String merchantID;
    String merchantName;
    String merchantIconURL;
    String merchantURL;


}