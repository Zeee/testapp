package uk.co.radicas.android;


import javax.inject.Singleton;

import dagger.Component;
import uk.co.radicas.android.ui.MainActivity;
import uk.co.radicas.android.ui.StartActivity;
import uk.co.radicas.android.modules.AppModule;
import uk.co.radicas.android.modules.DataModule;
import uk.co.radicas.android.modules.NetworkModule;
import uk.co.radicas.android.ui.onboarding.register.RegisterFragment;

@Singleton
@Component(modules = {AppModule.class, NetworkModule.class, DataModule.class})
public interface AppComponent {

    void inject(MainActivity activity);
    void inject(StartActivity activity);
    void inject(RegisterFragment fragment);
}
