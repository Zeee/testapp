package uk.co.radicas.android.ui;


import android.os.Bundle;
import android.support.annotation.Nullable;

import javax.inject.Inject;

import uk.co.radicas.android.ui.base.BaseActivity;
import uk.co.radicas.android.VoucherApp;
import uk.co.radicas.android.repo.Repository;
import uk.co.radicas.android.ui.onboarding.OnboardingActivity;

public class StartActivity extends BaseActivity {

    @Inject
    Repository repository;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        VoucherApp.getApp().getComponent().inject(this);

        if(repository.isUserLoggedIn() || repository.hasUserSkippedSignUp()){
            goToApp();
        } else {
            goToOnBoarding();
        }
        finish();
    }

    private void goToOnBoarding(){
        startActivity(OnboardingActivity.getIntent(this));
    }

    private void goToApp(){
        startActivity(MainActivity.getIntent(this));
    }

}
