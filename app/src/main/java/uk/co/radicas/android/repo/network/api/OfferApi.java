package uk.co.radicas.android.repo.network.api;


import org.json.JSONObject;

import java.util.List;

import io.reactivex.Single;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;
import uk.co.radicas.android.repo.network.request.GeoFenceLogRequest;
import uk.co.radicas.android.repo.data.model.Offer;
import uk.co.radicas.android.repo.network.request.GeoFenceRequest;
import uk.co.radicas.android.repo.network.request.GetCommentsRequest;
import uk.co.radicas.android.repo.network.request.OfferLikeRequest;
import uk.co.radicas.android.repo.network.request.RateOfferRequest;
import uk.co.radicas.android.repo.network.response.CommentResponse;
import uk.co.radicas.android.repo.network.response.GeocodeLookupResponse;
import uk.co.radicas.android.repo.network.response.OfferCodeResponse;
import uk.co.radicas.android.repo.network.response.OfferListResponse;
import uk.co.radicas.android.repo.network.response.OfferOutletListResponse;
import uk.co.radicas.android.repo.network.response.OutclickResponse;

public interface OfferApi {

    @GET("offer/getlist")
    Single<OfferListResponse> getList(@Query("list") String list, @Query("listID") String listId, @Query("lat") String latitude, @Query("lng") String longitude, @Query("radius") int radius, @Query("offset") int offsetKey, @Query("limit") int limit, @Query("testID") int testId, @Query("testVersion") int testVersion);

    @GET("offer/getoutlets")
    Single<OfferOutletListResponse> getStoresWhereOfferCanBeUsed(@Query("offerID") String offerId, @Query("lat") String latitude, @Query("lng") String longitude, @Query("offset") int offsetKey, @Query("limit") int limit);

    @GET("offer/getoutclick")
    Single<OutclickResponse> getOutClick(@Query("offerID") String offerId, @Query("sessionID") String sessionId);

    @GET("offer/get")
    Single<Offer> getOffer(@Query("offerID") String offerId, @Query("lat") String latitude, @Query("lng") String longitude);

    @GET("offer/getcode")
    Single<OfferCodeResponse> redeemCode(@Query("offerID") String offerId);

    @POST("offer/like")
    Single<Void> likeOffer(@Body OfferLikeRequest offerLikeRequest);

    @POST("offer/rate")
    Single<Void> rateOffer(@Body RateOfferRequest rateOfferRequest);

    @GET("geocode/lookup")
    Single<GeocodeLookupResponse> getGeocode(@Query("type") String type, @Query("query") String query);

    @GET("search/all")
    Single<List<JSONObject>> search(@Query("searchHint") String searchHint, @Query("lat") String latitude, @Query("lng") String longitude, @Query("testID") int testId, @Query("testVersion") int testVersion);

    @GET("offer/getcomments")
    Single<CommentResponse> getComments(@Query("offerID") String offerId, @Query("offset") int offsetKey, @Query("limit") int limit);

    @POST("offer/getcomments")
    Single<Void> getComments(@Body GetCommentsRequest getCommentsRequest);

    @POST("geofence/enter")
    Single<Void> enterGeofence(@Body GeoFenceRequest geoFenceRequest);

    @POST("geofence/leave")
    Single<Void> leaveGeofence(@Body GeoFenceRequest geoFenceRequest);

    @POST("geofence/log")
    Single<Void> logGeofence(@Body GeoFenceLogRequest geoFenceLogRequest);




}
