package uk.co.radicas.android.repo.data.model;


import io.realm.RealmObject;

public class SearchSection extends RealmObject {

    public String classType;
    public String sectionName;
    public int totalResults;
    public SearchHint searchHint;
}
