package uk.co.radicas.android.repo.network.request;


public class UserSignupRequest {

    private String name;
    private String email;
    private String password;

    public UserSignupRequest(String name, String email, String password) {
        this.name = name;
        this.email = email;
        this.password = password;
    }
}
