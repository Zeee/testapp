package uk.co.radicas.android.repo.data;


import android.content.SharedPreferences;

import java.util.List;

import io.realm.Realm;
import uk.co.radicas.android.repo.data.model.Merchant;
import uk.co.radicas.android.repo.data.model.UserInfo;
import uk.co.radicas.android.repo.data.model.Category;
import uk.co.radicas.android.repo.data.model.Offer;
import uk.co.radicas.android.repo.data.model.PopularSearch;

public class DataStore implements IDataStore {

    private SharedPreferences preferences;
    private Realm realm;

    public DataStore(SharedPreferences sharedPreferences, Realm realm) {
        this.preferences = sharedPreferences;
        this.realm = realm;
    }

    @Override
    public void saveAuthToken(String authToken){
        setString("VC_AUTH_TOKEN", authToken);
    }

    @Override
    public String getAuthToken(){
        return getString("VC_AUTH_TOKEN", null);
    }

    @Override
    public void saveAuthTokenSecret(String authTokenSecret){
        setString("VC_AUTH_TOKEN_SECRET", authTokenSecret);
    }

    @Override
    public String getAuthTokenSecret(){
        return getString("VC_AUTH_TOKEN_SECRET", null);
    }

    @Override
    public void setFirstLaunchComplete() {
        setBoolean("FIRST_LAUNCH", false);
    }

    @Override
    public boolean isFirstLaunch() {
        return getBoolean("FIRST_LAUNCH", true);
    }

    @Override
    public void setUserLoggedIn() {
        setBoolean("USER_LOGGED_IN", true);
    }

    @Override
    public boolean isUserLoggedIn() {
        return getBoolean("USER_LOGGED_IN", false);
    }

    @Override
    public void setUserSkippedSignUp() {
        setBoolean("SIGN_UP_SKIPPED", true);
    }

    @Override
    public boolean hasUserSkippedSignUp() {
        return getBoolean("SIGN_UP_SKIPPED", false);
    }

    @Override
    public List<Offer> getListOfOffers() {
        return realm.where(Offer.class).findAll();
    }

    @Override
    public void saveUserData(UserInfo userInfo) {

    }

    @Override
    public void saveCategories(List<Category> categories) {
        realm.insertOrUpdate(categories);
    }

    @Override
    public void saveMerchants(List<Merchant> merchantList) {
        realm.insertOrUpdate(merchantList);
    }

    @Override
    public void savePopularSearches(List<PopularSearch> searches) {
        realm.insertOrUpdate(searches);
    }


    /**
     * Helper methods for storing/retrieving preferences data
     */

    private SharedPreferences.Editor edit(){
        return preferences.edit();
    }

    private void setString(String key, String value){
        edit().putString(key, value).commit();
    }

    private String getString(String key, String defValue){
        return preferences.getString(key, defValue);
    }

    private void setBoolean(String key, boolean value){
        edit().putBoolean(key, value).commit();
    }

    private boolean getBoolean(String key, boolean defValue){
        return preferences.getBoolean(key, defValue);
    }
}
