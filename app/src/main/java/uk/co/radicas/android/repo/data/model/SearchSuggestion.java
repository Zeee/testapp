package uk.co.radicas.android.repo.data.model;


import io.realm.RealmObject;

public class SearchSuggestion extends RealmObject {
    String classType;
    String title;
    String iconURL;

    public static class SearchHint {
        String merchantID;
    }
}
