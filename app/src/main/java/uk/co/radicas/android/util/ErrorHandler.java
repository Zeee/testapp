package uk.co.radicas.android.util;


import retrofit2.HttpException;

public class ErrorHandler {

    public static String getErrorMessage(Throwable throwable){
        String errorMessage = "";

        if(throwable instanceof HttpException){
            errorMessage = ((HttpException) throwable).message();
        } else if(throwable instanceof IllegalArgumentException){
            errorMessage = "User input error";
        } else if(throwable instanceof Exception) {
            errorMessage = "Something went wrong";
        }

        throwable.printStackTrace();

        return errorMessage;
    }
}
