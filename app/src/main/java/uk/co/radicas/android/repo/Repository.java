package uk.co.radicas.android.repo;


import java.util.ArrayList;
import java.util.List;

import io.reactivex.Single;
import uk.co.radicas.android.repo.data.IDataStore;
import uk.co.radicas.android.repo.data.model.Category;
import uk.co.radicas.android.repo.data.model.Merchant;
import uk.co.radicas.android.repo.data.model.UserInfo;
import uk.co.radicas.android.repo.network.api.OfferApi;
import uk.co.radicas.android.repo.network.api.PreAuthApi;
import uk.co.radicas.android.repo.network.api.UserApi;
import uk.co.radicas.android.repo.network.request.UserSignupRequest;
import uk.co.radicas.android.repo.data.model.PopularSearch;
import uk.co.radicas.android.repo.network.api.DeviceApi;
import uk.co.radicas.android.repo.network.api.ReportingApi;
import uk.co.radicas.android.repo.network.request.AuthorisationRequest;

public class Repository implements IRepository {

    private PreAuthApi preAuthApi;
    private DeviceApi deviceApi;
    private OfferApi offerApi;
    private ReportingApi reportingApi;
    private UserApi userApi;
    private IDataStore dataStore;

    public Repository(PreAuthApi preAuthApi, DeviceApi deviceApi, OfferApi offerApi, ReportingApi reportingApi, UserApi userApi, IDataStore dataStore) {
        this.preAuthApi = preAuthApi;
        this.deviceApi = deviceApi;
        this.offerApi = offerApi;
        this.reportingApi = reportingApi;
        this.userApi = userApi;
        this.dataStore = dataStore;
    }

    public boolean isUserLoggedIn(){
        return dataStore.isUserLoggedIn();
    }

    public boolean hasUserSkippedSignUp(){
        return dataStore.hasUserSkippedSignUp();
    }

    @Override
    public Single<Boolean> preAuthDevice(){
        return preAuthApi.generateToken()
                .flatMap(tokenResponse -> {
                    if(tokenResponse == null || tokenResponse.getToken() == null || tokenResponse.getToken().isEmpty()){
                        throw new NullPointerException("Auth token is null");
                    }

                    String token = tokenResponse.getToken();
                    AuthorisationRequest authorisationRequest = new AuthorisationRequest(token);

                    return preAuthApi.authoriseToken(authorisationRequest);
                })
                .flatMap(authorisationResponse -> {
                    if(authorisationResponse == null || authorisationResponse.getOauthToken() == null || authorisationResponse.getOauthTokenSecret() == null){
                        throw new NullPointerException("Client authentication failed");
                    }

                    String oAuthToken = authorisationResponse.getOauthToken();
                    String oAuthTokenSecret = authorisationResponse.getOauthTokenSecret();

                    dataStore.saveAuthToken(oAuthToken);
                    dataStore.saveAuthTokenSecret(oAuthTokenSecret);

                    return fetchMetadata();
                });
    }


    private Single<UserInfo> authenticate(String username, String password){
        return generateToken()
            .flatMap(this::authoriseToken)
            .flatMap(it -> fetchMetadata())
            .flatMap(it-> registerNewUser(username, password));
    }


    @Override
    public Single<AuthorisationRequest> generateToken(){
        return preAuthApi.generateToken()
                .map(tokenResponse -> {
                    if(tokenResponse == null || tokenResponse.getToken() == null || tokenResponse.getToken().isEmpty()){
                        throw new NullPointerException("Auth token is null");
                    }

                    String token = tokenResponse.getToken();
                    return new AuthorisationRequest(token);
                });
    }


    @Override
    public Single<Void> authoriseToken(AuthorisationRequest authorisationRequest){
        return preAuthApi.authoriseToken(authorisationRequest)
                .map(authorisationResponse -> {
                    if(authorisationResponse == null || authorisationResponse.getOauthToken() == null || authorisationResponse.getOauthTokenSecret() == null){
                        throw new NullPointerException("Client authentication failed");
                    }

                    String oAuthToken = authorisationResponse.getOauthToken();
                    String oAuthTokenSecret = authorisationResponse.getOauthTokenSecret();

                    dataStore.saveAuthToken(oAuthToken);
                    dataStore.saveAuthTokenSecret(oAuthTokenSecret);

                    return null;
                });
    }

    @Override
    public Single<Boolean> fetchMetadata() {
        return preAuthApi.getAppMetaData()
                .map(metaDataResponse -> {
                    if(metaDataResponse == null || metaDataResponse.getOnlineCategoryList() == null || metaDataResponse.getInstoreCategoryList() == null
                            || metaDataResponse.getPopularMerchantList() == null || metaDataResponse.getPopularSearchList() == null){
                        throw new NullPointerException("Incomplete meta-data was returned");
                    }

                    List<Category> inStoreCategories = metaDataResponse.getInstoreCategoryList();
                    List<Category> onlineCategories = metaDataResponse.getOnlineCategoryList();
                    List<Merchant> merchantList = metaDataResponse.getPopularMerchantList();
                    List<PopularSearch>  searches = metaDataResponse.getPopularSearchList();

                    List<Category> categories = new ArrayList<>();
                    categories.addAll(inStoreCategories);
                    categories.addAll(onlineCategories);

                    dataStore.saveCategories(categories);
                    dataStore.saveMerchants(merchantList);
                    dataStore.savePopularSearches(searches);

                    return true;
                });
    }

    @Override
    public Single<UserInfo> registerNewUser(String email, String password){
        return userApi.signUp(new UserSignupRequest("name", email, password))
                .map(userInfoResponse -> {
                    if(userInfoResponse != null && userInfoResponse.getUserInfo() != null){
                        return userInfoResponse.getUserInfo();
                    } else {
                        throw new NullPointerException("Received invalid data from backend");
                    }
                });
    }
}
