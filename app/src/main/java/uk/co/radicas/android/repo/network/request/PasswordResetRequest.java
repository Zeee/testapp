package uk.co.radicas.android.repo.network.request;


public class PasswordResetRequest {

    private String email;

    public PasswordResetRequest(String email) {
        this.email = email;
    }
}
