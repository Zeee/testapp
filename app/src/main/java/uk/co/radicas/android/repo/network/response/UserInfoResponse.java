package uk.co.radicas.android.repo.network.response;


import uk.co.radicas.android.repo.data.model.UserInfo;

public class UserInfoResponse {
    UserInfo userInfo;

    public UserInfo getUserInfo() {
        return userInfo;
    }
}
