package uk.co.radicas.android.ui.onboarding.register;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.jakewharton.rxbinding2.view.RxView;
import com.jakewharton.rxbinding2.widget.RxTextView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import uk.co.radicas.android.ui.base.BaseFragment;
import uk.co.radicalapps.android.R;
import uk.co.radicas.android.VoucherApp;
import uk.co.radicas.android.repo.Repository;
import uk.co.radicas.android.ui.MainActivity;


public class RegisterFragment extends BaseFragment implements RegisterPresenter.View {

    public static RegisterFragment newInstance() {

        Bundle args = new Bundle();

        RegisterFragment fragment = new RegisterFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Inject
    Repository repository;

    private RegisterPresenter presenter;

    @BindView(R.id.register_button) Button registerButton;
    @BindView(R.id.register_email_address) EditText emailEditText;
    @BindView(R.id.register_password) EditText passwordEditText;
    private ProgressDialog progressDialog;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_register, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        VoucherApp.getApp().getComponent().inject(this);

        ButterKnife.bind(this, view);

        presenter = new RegisterPresenter(repository, Schedulers.io(), AndroidSchedulers.mainThread());

        progressDialog = new ProgressDialog(view.getContext());
        progressDialog.setMessage(getResources().getString(R.string.registering));
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
    }

    @Override
    public void onStart() {
        super.onStart();
        presenter.attachView(this);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        presenter.detachView();
    }

    @Override
    public void onGenericError(String error) {

    }

    @Override
    public Observable<Object> getOnRegisterClickObs() {
        return RxView.clicks(registerButton);
    }

    @Override
    public Observable<String> getOnEmailTextChangeObs() {
        return RxTextView.textChanges(emailEditText).map(String::valueOf);
    }

    @Override
    public Observable<String> getOnPasswordTextChangeObs() {
        return RxTextView.textChanges(passwordEditText).map(String::valueOf);
    }

    @Override
    public void goToMainActivity() {
        startActivity(MainActivity.getIntent(getContext()));
        if(getActivity() != null) {
            getActivity().finish();
        }
    }

    @Override
    public void showLoading(boolean show) {
        if(show){
            progressDialog.show();
        } else {
            progressDialog.dismiss();
        }
    }

    @Override
    public void enableRegistrationButton(boolean enable) {
        registerButton.setEnabled(enable);
    }

    @Override
    public void showEmailError(boolean show) {
        String error = show ? "Email is invalid" : null;
        emailEditText.setError(error);
    }

    @Override
    public void showPasswordError(boolean show) {
        String error = show ? "Password is invalid" : null;
        passwordEditText.setError(error);
    }

    @Override
    public void showRegistrationError(String error) {
        Toast.makeText(getContext(), error, Toast.LENGTH_SHORT).show();
    }
}
