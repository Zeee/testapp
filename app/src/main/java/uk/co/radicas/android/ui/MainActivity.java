package uk.co.radicas.android.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import javax.inject.Inject;

import uk.co.radicas.android.VoucherApp;
import uk.co.radicalapps.android.R;
import uk.co.radicas.android.repo.Repository;

public class MainActivity extends AppCompatActivity {

    @Inject
    Repository repository;

    public static Intent getIntent(Context context){
        return new Intent(context, MainActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        VoucherApp.getApp().getComponent().inject(this);
    }
}
