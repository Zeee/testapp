package uk.co.radicas.android.repo.network.response;


import com.google.gson.annotations.SerializedName;

import java.util.List;

import uk.co.radicas.android.repo.data.model.Comment;

public class CommentResponse {

    int totalResults;
    @SerializedName("data")
    List<Comment> comments;
}
