package uk.co.radicas.android.repo.network.response;


import com.google.gson.annotations.SerializedName;

import java.util.List;

import uk.co.radicas.android.repo.data.model.Offer;

public class OfferListResponse {

    int totalResults;
    @SerializedName("data")
    List<Offer> offers;
}
