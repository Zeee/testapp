package uk.co.radicas.android.repo.network.response;


import uk.co.radicas.android.repo.data.model.UserInfo;

public class AuthorisationResponse {

    String oauthToken;
    String oauthTokenSecret;
    UserInfo userInfo;
    ActiveTests activeTests;

    public String getOauthToken() {
        return oauthToken;
    }

    public String getOauthTokenSecret() {
        return oauthTokenSecret;
    }

    public UserInfo getUserInfo() {
        return userInfo;
    }

    public ActiveTests getActiveTests() {
        return activeTests;
    }

    public final class ActiveTests {
        boolean zeroClick;

        public boolean isZeroClick() {
            return zeroClick;
        }
    }
}
