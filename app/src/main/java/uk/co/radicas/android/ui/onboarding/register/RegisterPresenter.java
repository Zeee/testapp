package uk.co.radicas.android.ui.onboarding.register;


import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.Scheduler;
import io.reactivex.disposables.Disposable;
import uk.co.radicas.android.ui.base.BaseView;
import uk.co.radicas.android.repo.IRepository;
import uk.co.radicas.android.ui.base.BasePresenter;
import uk.co.radicas.android.util.ErrorHandler;
import uk.co.radicas.android.util.ValidationUtils;

public class RegisterPresenter extends BasePresenter<RegisterPresenter.View> {

    private IRepository repository;

    private String email;
    private String password;


    public RegisterPresenter(IRepository repository, Scheduler ioScheduler, Scheduler uiScheduler) {
        super(ioScheduler, uiScheduler);
        this.repository = repository;
    }

    @Override
    public void attachView(View view) {
        super.attachView(view);

        addSubscription(view.getOnRegisterClickObs()
                .subscribe(object -> {
                    getView().showLoading(true);
                    registerUser();
                }));

        addSubscription(getFormValidationDisposable(view));
    }


    private void registerUser(){
        repository.preAuthDevice()
                .subscribeOn(getIoScheduler())
                .flatMap(noValue -> repository.registerNewUser(email, password))
                .observeOn(getUiScheduler())
                .subscribe(userInfo -> {
                    getView().showLoading(false);
                    getView().goToMainActivity();
                },
                throwable -> {
                    getView().showRegistrationError(ErrorHandler.getErrorMessage(throwable));
                    getView().showLoading(false);
                });
    }

    private Disposable getFormValidationDisposable(View view){
        Observable<Boolean> emailObs = view.getOnEmailTextChangeObs()
                .debounce(500, TimeUnit.MILLISECONDS, getIoScheduler())
                .map(email -> {
                    boolean isValid = ValidationUtils.isEmailValid(email);
                    RegisterPresenter.this.email = isValid ? email : null;
                    return isValid;
                });

        Observable<Boolean> passwordObs = view.getOnPasswordTextChangeObs()
                .debounce(500, TimeUnit.MILLISECONDS, getIoScheduler())
                .map(password -> {
                    boolean isValid = ValidationUtils.isPasswordValid(password);
                    RegisterPresenter.this.password = isValid ? password : null;
                    return isValid;
                });

        emailObs
                .skip(1)
                .observeOn(getUiScheduler())
                .subscribe(isValid -> getView().showEmailError(!isValid));

        passwordObs
                .skip(1)
                .observeOn(getUiScheduler())
                .subscribe(isValid -> getView().showPasswordError(!isValid));

        return Observable
                .combineLatest(
                        emailObs,
                        passwordObs,
                        (isEmailValid, isPasswordValid) -> isEmailValid && isPasswordValid)
                .observeOn(getUiScheduler())
                .subscribe(enable -> getView().enableRegistrationButton(enable));
    }


    public interface View extends BaseView {
        Observable<Object> getOnRegisterClickObs();
        Observable<String> getOnEmailTextChangeObs();
        Observable<String> getOnPasswordTextChangeObs();

        void goToMainActivity();
        void showLoading(boolean show);
        void enableRegistrationButton(boolean enable);
        void showEmailError(boolean show);
        void showPasswordError(boolean show);
        void showRegistrationError(String error);
    }
}
