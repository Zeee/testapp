package uk.co.radicas.android.repo.data.model;


import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class UserInfo extends RealmObject {

    @PrimaryKey
    int id = 1;
    String userID;
    String firstName;
    String lastName;
    String emailAddress;
    String postCode;
    RealmList<String> favouriteMerchants;

    public int getId() {
        return id;
    }

    public String getUserID() {
        return userID;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public String getPostCode() {
        return postCode;
    }

    public RealmList<String> getFavouriteMerchants() {
        return favouriteMerchants;
    }
}


