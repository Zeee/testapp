package uk.co.radicas.android.repo.network.api;


import io.reactivex.Single;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import uk.co.radicas.android.repo.network.request.CheckEmailRequest;
import uk.co.radicas.android.repo.network.request.MerchantRequest;
import uk.co.radicas.android.repo.network.request.SaveOfferRequest;
import uk.co.radicas.android.repo.network.request.SocialLoginRequest;
import uk.co.radicas.android.repo.network.request.UserSignupRequest;
import uk.co.radicas.android.repo.network.response.CheckEmailResponse;
import uk.co.radicas.android.repo.network.response.SavedOfferResponse;
import uk.co.radicas.android.repo.network.response.UserInfoResponse;
import uk.co.radicas.android.repo.network.request.LoginRequest;
import uk.co.radicas.android.repo.network.request.PasswordResetRequest;
import uk.co.radicas.android.repo.network.response.PasswordResetResponse;

public interface UserApi {

    @GET("user/info")
    Single<UserInfoResponse> getUserInfo();

    @POST("user/signup")
    Single<UserInfoResponse> signUp(@Body UserSignupRequest userSignupRequest);

    @POST("user/sociallogin")
    Single<UserInfoResponse> socialLogin(@Body SocialLoginRequest socialLoginRequest);

    @POST("user/login")
    Single<UserInfoResponse> login(@Body LoginRequest loginRequest);

    @POST("user/passwordreset")
    Single<PasswordResetResponse> resetPassword(@Body PasswordResetRequest passwordResetRequest);

    @POST("user/favoritedmerchants")
    Single<Void> saveFavouriteMerchant(@Body MerchantRequest merchantRequest);

    @POST("user/favoritedmerchants")
    Single<Void> putFavouriteMerchant(@Body MerchantRequest merchantRequest);

    @DELETE("user/favoritedmerchants")
    Single<Void> removeFavouriteMerchant(@Body MerchantRequest merchantRequest);

    @POST("user/savedoffers")
    Single<Void> saveOffer(@Body SaveOfferRequest saveOfferRequest);

    @GET("user/savedoffers")
    Single<SavedOfferResponse> getSavedOffer();

    @DELETE("user/savedoffers/{offerID}")
    Single<Void> removeSavedOffer(@Path("offerID") String offerId);

    @POST("user/checkemail")
    Single<CheckEmailResponse> checkUserEmailStatus(@Body CheckEmailRequest checkEmailRequest);



}
