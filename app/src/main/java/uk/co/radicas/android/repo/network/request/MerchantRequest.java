package uk.co.radicas.android.repo.network.request;


public class MerchantRequest {

    private String merchantID;

    public MerchantRequest(String merchantId) {
        this.merchantID = merchantId;
    }
}
