package uk.co.radicas.android.repo.data.model;


import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;

public class PopularSearch extends RealmObject {

    @PrimaryKey
    @Required
    String searchName;
    SearchHint searchHint;
}
