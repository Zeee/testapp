package uk.co.radicas.android.repo.network.request;


public class GeoFenceRequest {

    private String timestamp;
    private String dwellTime;
    private String fenceLocalID;
    private String visitID;

    public GeoFenceRequest(String timestamp, String dwellTime, String fenceLocalId, String visitId) {
        this.timestamp = timestamp;
        this.dwellTime = dwellTime;
        this.fenceLocalID = fenceLocalId;
        this.visitID = visitId;
    }
}
