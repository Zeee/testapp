package uk.co.radicas.android.repo.data.model;


import io.realm.RealmObject;

public class SearchHint extends RealmObject {
    String merchantID;
    public String query;
    public String type;
}
