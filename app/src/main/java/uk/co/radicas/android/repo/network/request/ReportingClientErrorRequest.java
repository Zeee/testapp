package uk.co.radicas.android.repo.network.request;


public class ReportingClientErrorRequest {

    private String payload;

    public ReportingClientErrorRequest(String payload) {
        this.payload = payload;
    }
}
