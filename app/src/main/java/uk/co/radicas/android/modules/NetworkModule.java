package uk.co.radicas.android.modules;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.ihsanbal.logging.Level;
import com.ihsanbal.logging.LoggingInterceptor;

import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import uk.co.radicas.android.repo.data.DataStore;
import uk.co.radicas.android.repo.network.api.OfferApi;
import uk.co.radicas.android.repo.network.api.PreAuthApi;
import uk.co.radicas.android.repo.network.api.UserApi;
import uk.co.radicas.android.repo.network.auth.OAuthInterceptor;
import uk.co.radicalapps.android.BuildConfig;
import uk.co.radicas.android.repo.network.api.DeviceApi;
import uk.co.radicas.android.repo.network.api.ReportingApi;
import uk.co.radicas.android.repo.network.auth.OkHttpOAuthConsumer;

@Module
public class NetworkModule {

    private String baseUrl;

    public NetworkModule(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    @Provides
    @Singleton
    OkHttpOAuthConsumer provideOAuthConsumer() {
        return new OkHttpOAuthConsumer(BuildConfig.OAUTH_CONSUMER_KEY, BuildConfig.OAUTH_CONSUMER_SECRET);
    }

    @Provides
    @Singleton
    OAuthInterceptor provideOauthInterceptor(DataStore dataStore, OkHttpOAuthConsumer okHttpOAuthConsumer) {
        return new OAuthInterceptor(BuildConfig.BASE_URL, dataStore, okHttpOAuthConsumer);
    }

    @Provides
    @Singleton
    Gson provideGson() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        return gsonBuilder.create();
    }

    @Provides
    @Singleton
    OkHttpClient provideOkhttpClient(OAuthInterceptor oauthInterceptor) {
        OkHttpClient.Builder okHttpClientBuilder = new OkHttpClient.Builder()
                .connectTimeout(10, TimeUnit.SECONDS)
                .readTimeout(10, TimeUnit.SECONDS);

        okHttpClientBuilder.interceptors().add(oauthInterceptor);

        LoggingInterceptor loggingInterceptor = new LoggingInterceptor.Builder()
                .loggable(BuildConfig.DEBUG)
                .setLevel(Level.BODY)
                .request("Request")
                .response("Response")
                .build();


        okHttpClientBuilder.addInterceptor(loggingInterceptor);

        return okHttpClientBuilder.build();
    }

    @Provides
    @Singleton
    Retrofit provideRetrofit(Gson gson, OkHttpClient okHttpClient) {
        return new Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
    }

    @Provides
    @Singleton
    PreAuthApi providePreAuthApi(Retrofit retrofit){
        return retrofit.create(PreAuthApi.class);
    }

    @Provides
    @Singleton
    DeviceApi provideDeviceApi(Retrofit retrofit){
        return retrofit.create(DeviceApi.class);
    }

    @Provides
    @Singleton
    OfferApi provideOfferApi(Retrofit retrofit){
        return retrofit.create(OfferApi.class);
    }

    @Provides
    @Singleton
    ReportingApi provideReportingApi(Retrofit retrofit){
        return retrofit.create(ReportingApi.class);
    }

    @Provides
    @Singleton
    UserApi provideUserApi(Retrofit retrofit){
        return retrofit.create(UserApi.class);
    }



}
