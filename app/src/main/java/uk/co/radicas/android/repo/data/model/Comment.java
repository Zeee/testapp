package uk.co.radicas.android.repo.data.model;


import io.realm.RealmObject;

public class Comment extends RealmObject {

    String offerID;
    String comment;
    String userName;
    String dateAdded;
    boolean isSupportTeam;
    int parentOfferCommentTotal;
}
