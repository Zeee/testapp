package uk.co.radicas.android.repo.network.request;


public class CheckEmailRequest {

    private String email;

    public CheckEmailRequest(String email) {
        this.email = email;
    }
}
