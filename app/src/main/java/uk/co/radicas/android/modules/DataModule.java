package uk.co.radicas.android.modules;

import android.content.Context;
import android.content.SharedPreferences;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.realm.Realm;
import uk.co.radicas.android.repo.data.DataStore;
import uk.co.radicas.android.repo.network.api.OfferApi;
import uk.co.radicas.android.repo.network.api.PreAuthApi;
import uk.co.radicas.android.repo.Repository;
import uk.co.radicas.android.repo.network.api.DeviceApi;
import uk.co.radicas.android.repo.network.api.ReportingApi;
import uk.co.radicas.android.repo.network.api.UserApi;

@Module (includes = {AppModule.class, NetworkModule.class})
public class DataModule {

    @Provides
    @Singleton
    public Realm provideRealm(){
        return Realm.getDefaultInstance();
    }

    @Provides
    @Singleton
    public SharedPreferences provideSharedPreferences(Context context){
        return context.getSharedPreferences("VC_SHARED_PREFS", Context.MODE_PRIVATE);
    }

    @Provides
    @Singleton
    public DataStore providesDataStore(SharedPreferences sharedPreferences, Realm realm){
        return new DataStore(sharedPreferences, realm);
    }

    @Provides
    @Singleton
    Repository provideRepository(PreAuthApi preAuthApi, DeviceApi deviceApi, OfferApi offerApi, ReportingApi reportingApi, UserApi userApi, DataStore dataStore){
        return new Repository(preAuthApi, deviceApi, offerApi, reportingApi, userApi, dataStore);
    }

}
