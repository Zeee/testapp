package uk.co.radicas.android.repo;


import io.reactivex.Single;
import uk.co.radicas.android.repo.data.model.UserInfo;
import uk.co.radicas.android.repo.network.request.AuthorisationRequest;

public interface IRepository {

    Single<Boolean> preAuthDevice();
    Single<AuthorisationRequest> generateToken();
    Single<Void> authoriseToken(AuthorisationRequest authorisationRequest);
    Single<Boolean> fetchMetadata();
    Single<UserInfo> registerNewUser(String email, String password);
}
