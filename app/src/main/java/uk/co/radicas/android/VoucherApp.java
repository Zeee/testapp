package uk.co.radicas.android;

import android.app.Application;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import timber.log.Timber;
import uk.co.radicalapps.android.BuildConfig;
import uk.co.radicalapps.android.DaggerAppComponent;
import uk.co.radicas.android.modules.AppModule;
import uk.co.radicas.android.modules.NetworkModule;


public class VoucherApp extends Application {

    private static VoucherApp app;
    private AppComponent component;

    public static VoucherApp getApp() {
        return app;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        app = this;

        configureLogging();

        configureRealm();

        configureDagger();

        configureFirebase();
    }

    private void configureFirebase() {
        //FirebaseApp.initializeApp(this);
    }

    public AppComponent getComponent() {
        return component;
    }

    private void configureRealm(){
        Realm.init(this);

        RealmConfiguration configuration = new RealmConfiguration.Builder()
                .name("vc_db_realm")
                .schemaVersion(1)
                .build();

        Realm.setDefaultConfiguration(configuration);
    }

    private void configureLogging(){
        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        }
    }

    private void configureDagger(){
        component = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .networkModule(new NetworkModule(BuildConfig.BASE_URL))
                .build();
    }
}
