package uk.co.radicas.android.repo.network.api;


import io.reactivex.Single;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import uk.co.radicas.android.repo.network.response.MetaDataResponse;
import uk.co.radicas.android.repo.network.request.AuthorisationRequest;
import uk.co.radicas.android.repo.network.response.AuthorisationResponse;
import uk.co.radicas.android.repo.network.response.TokenResponse;

public interface PreAuthApi {

    @GET("token/generate")
    Single<TokenResponse> generateToken();

    @POST("token/authorize")
    Single<AuthorisationResponse> authoriseToken(@Body AuthorisationRequest authorisationRequest);

    @GET("metadata/all")
    Single<MetaDataResponse> getAppMetaData();
}
