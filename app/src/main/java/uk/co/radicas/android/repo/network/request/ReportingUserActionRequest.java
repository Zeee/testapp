package uk.co.radicas.android.repo.network.request;


public class ReportingUserActionRequest {

    private String buttonID;
    private String description;
    private String value;

    public ReportingUserActionRequest(String buttonId, String description, String value) {
        this.buttonID = buttonId;
        this.description = description;
        this.value = value;
    }
}
