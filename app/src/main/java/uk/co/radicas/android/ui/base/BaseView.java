package uk.co.radicas.android.ui.base;


public interface BaseView {
    void onGenericError(String error);
}
