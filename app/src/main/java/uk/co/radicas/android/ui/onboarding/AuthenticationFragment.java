package uk.co.radicas.android.ui.onboarding;

import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.BindView;
import butterknife.ButterKnife;
import uk.co.radicas.android.ui.base.BaseFragment;
import uk.co.radicalapps.android.R;
import uk.co.radicas.android.ui.onboarding.login.LoginFragment;
import uk.co.radicas.android.ui.onboarding.register.RegisterFragment;


public class AuthenticationFragment extends BaseFragment {

    public static AuthenticationFragment newInstance() {

        Bundle args = new Bundle();

        AuthenticationFragment fragment = new AuthenticationFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @BindView(R.id.authentication_tablayout) TabLayout tabLayout;
    @BindView(R.id.authentication_viewpager) ViewPager viewPager;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_authentication, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ButterKnife.bind(this, view);

        viewPager.setAdapter(new AuthPagerAdapter(getChildFragmentManager(), getResources()));
        tabLayout.setupWithViewPager(viewPager);
    }



    private static class AuthPagerAdapter extends FragmentPagerAdapter {

        private Resources resources;

        AuthPagerAdapter(FragmentManager fm, Resources resources) {
            super(fm);
            this.resources = resources;
        }

        @Override
        public Fragment getItem(int position) {
            if(position == 0){
                return RegisterFragment.newInstance();
            } else {
                return LoginFragment.newInstance();
            }
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Nullable
        @Override
        public CharSequence getPageTitle(int position) {
            if(position == 0){
                return resources.getString(R.string.register);
            } else {
                return resources.getString(R.string.login);
            }
        }
    }

}
