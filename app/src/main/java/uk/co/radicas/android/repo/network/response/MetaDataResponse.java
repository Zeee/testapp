package uk.co.radicas.android.repo.network.response;


import com.google.gson.annotations.SerializedName;

import java.util.List;

import uk.co.radicas.android.repo.data.model.Merchant;
import uk.co.radicas.android.repo.data.model.Category;
import uk.co.radicas.android.repo.data.model.PopularSearch;

public class MetaDataResponse {

    @SerializedName("InstoreCategory")
    List<Category> instoreCategoryList;
    @SerializedName("OnlineCategory")
    List<Category> onlineCategoryList;
    @SerializedName("PopularSearches")
    List<PopularSearch> popularSearchList;
    @SerializedName("PopularMerchants")
    List<Merchant> popularMerchantList;

    public List<Category> getInstoreCategoryList() {
        return instoreCategoryList;
    }

    public List<Category> getOnlineCategoryList() {
        return onlineCategoryList;
    }

    public List<PopularSearch> getPopularSearchList() {
        return popularSearchList;
    }

    public List<Merchant> getPopularMerchantList() {
        return popularMerchantList;
    }
}
