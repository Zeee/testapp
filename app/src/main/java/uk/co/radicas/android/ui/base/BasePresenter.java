package uk.co.radicas.android.ui.base;


import io.reactivex.Scheduler;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

public class BasePresenter<V extends BaseView> implements IBasePresenter<V> {

    private final Scheduler uiScheduler, ioScheduler;
    private V view;
    private CompositeDisposable compositeDisposable;

    public BasePresenter(Scheduler ioScheduler, Scheduler uiScheduler) {
        this.uiScheduler = uiScheduler;
        this.ioScheduler = ioScheduler;
    }

    @Override
    public void attachView(V view) {
        makeSureViewIsNotNull(view);
        this.view = view;
        compositeDisposable = new CompositeDisposable();
    }

    @Override
    public void detachView() {
        if(compositeDisposable == null || view == null){
            throw new IllegalStateException("Cannot call detachView() before attachView(view)");
        }

        compositeDisposable.clear();
        view = null;
    }

    private boolean isViewAttached() {
        return view != null;
    }

    private void makeSureViewIsNotNull(V view){
        if(view == null){
            throw new NullPointerException("View cannot be null");
        }
    }

    protected final V getView() {
        return view;
    }

    protected final Scheduler getIoScheduler() {
        return ioScheduler;
    }

    protected final Scheduler getUiScheduler() {
        return uiScheduler;
    }

    public final void checkViewAttached() {
        if (!isViewAttached()) {
            throw new MvpViewNotAttachedException();
        }
    }

    protected void addSubscription(Disposable subscription) {
        this.compositeDisposable.add(subscription);
    }

    public static class MvpViewNotAttachedException extends RuntimeException {
        MvpViewNotAttachedException() {
            super("Please call Presenter.attachView(MvpView) before requesting data to the Presenter");
        }
    }
}
