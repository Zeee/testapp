package uk.co.radicas.android.repo.data.model;


import io.realm.RealmObject;

public class Offer extends RealmObject {
    
    String classType;
    boolean requiresSignIn;
    String offerID;
    int maxUses;
    int usesByUser;
    int numberOfLikes;
    int numberOfComments;
    String merchant;
    String merchantID;
    boolean isCommentingEnabled;
    String merchantIconURL;
    String backgroundImageURL;
    String codeType;
    boolean isFeatured;
    boolean isTopLockedPlacement;
    String offerTitle;
    String code;
    boolean isFetchedCode;
    String getCodeFailAction;
    String description;
    String miscellaneous;
    String shareURL;
    String offerURL;
    String offerType;
    int startDateTime;
    int endDateTime;
    boolean isExclusive;
    OfferOutlet outlet;
    int totalOutlets;

}
