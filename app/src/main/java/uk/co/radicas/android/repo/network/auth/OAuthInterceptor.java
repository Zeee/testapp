package uk.co.radicas.android.repo.network.auth;

import android.support.annotation.NonNull;

import java.io.IOException;

import oauth.signpost.exception.OAuthCommunicationException;
import oauth.signpost.exception.OAuthExpectationFailedException;
import oauth.signpost.exception.OAuthMessageSignerException;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;
import timber.log.Timber;
import uk.co.radicas.android.repo.data.DataStore;

public class OAuthInterceptor implements Interceptor {

    private DataStore dataStore;

    private OkHttpOAuthConsumer okHttpOAuthConsumer;

    private String apiHostname;

    public OAuthInterceptor(String baseUrl, DataStore dataStore, OkHttpOAuthConsumer okHttpOAuthConsumer) {
        this.dataStore = dataStore;
        this.okHttpOAuthConsumer = okHttpOAuthConsumer;
        this.apiHostname = baseUrl;
    }

    @Override
    public Response intercept(@NonNull Chain chain) throws IOException {
        String requestUrl = chain.request().url().url().toString();
        if (requestUrl.contains(apiHostname)) {
            String authToken = dataStore.getAuthToken();
            String tokenSecret = dataStore.getAuthTokenSecret();

            Request signedRequest = signRequest(chain, authToken, tokenSecret);

            return chain.proceed(signedRequest);
        } else {
            return chain.proceed(chain.request());
        }
    }

    private Request signRequest(Chain chain, String token, String tokenSecret) {
        okHttpOAuthConsumer.setTokenWithSecret(token, tokenSecret);
        Request request = chain.request();
        try {
            HttpRequestAdapter httpRequestAdapter = (HttpRequestAdapter) okHttpOAuthConsumer.sign(request);
            request = (Request) httpRequestAdapter.unwrap();
        } catch (OAuthMessageSignerException | OAuthCommunicationException | OAuthExpectationFailedException e) {
            Timber.e("Exception during sign oAuth %s", e.getMessage());
        }
        return request;
    }


}
