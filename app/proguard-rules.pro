# Add project specific ProGuard rules here.
# You can control the set of applied configuration files using the
# proguardFiles setting in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

# Uncomment this to preserve the line number information for
# debugging stack traces.
#-keepattributes SourceFile,LineNumberTable

# If you keep the line number information, uncomment this to
# hide the original source file name.
#-renamesourcefileattribute SourceFile


#---------------------------------------------------
# Retrofit
#---------------------------------------------------
# Platform calls Class.forName on types which do not exist on Android to determine platform.
-dontnote retrofit2.Platform
# Platform used when running on Java 8 VMs. Will not be used at runtime.
-dontwarn retrofit2.Platform$Java8
# Retain generic type information for use by reflection by converters and adapters.
-keepattributes Signature
# Retain declared checked exceptions for use by a Proxy instance.
-keepattributes Exceptions

-dontwarn okio.**

##---------------Begin: proguard configuration for Gson  ----------
# Gson uses generic type information stored in a class file when working with fields. Proguard
# removes such information by default, so configure it to keep all of it.
-keepattributes Signature

# For using GSON @Expose annotation
-keepattributes *Annotation*

# Gson specific classes
-dontwarn sun.misc.**
#-keep class com.google.gson.stream.** { *; }

# Application classes that will be serialized/deserialized over Gson
-keep class com.google.gson.examples.android.model.** { *; }

# Prevent proguard from stripping interface information from TypeAdapterFactory,
# JsonSerializer, JsonDeserializer instances (so they can be used in @JsonAdapter)
-keep class * implements com.google.gson.TypeAdapterFactory
-keep class * implements com.google.gson.JsonSerializer
-keep class * implements com.google.gson.JsonDeserializer

##---------------End: proguard configuration for Gson  ----------


#-------------------------------------------------
# Glide
#-------------------------------------------------

-keep public class * implements com.bumptech.glide.module.GlideModule
-keep public class * extends com.bumptech.glide.module.AppGlideModule
-keep public enum com.bumptech.glide.load.resource.bitmap.ImageHeaderParser$** {
  **[] $VALUES;
  public *;
}

-dontwarn com.bumptech.glide.load.resource.bitmap.VideoDecoder

# for DexGuard only
-keepresourcexmlelements manifest/application/meta-data@value=GlideModule

#-------------------------------------------------
# crashlytics
#-------------------------------------------------
-keep class com.crashlytics.** { *; }
-dontwarn com.crashlytics.**
-keepattributes *Annotation*, SourceFile, LineNumberTable

#-------------------------------------------------
# ExactTarget MarketingCloudSdk
#-------------------------------------------------
-dontwarn org.altbeacon.**

#-------------------------------------------------
# Support
#-------------------------------------------------
-dontwarn android.support.**
-keep class android.support.v7.widget.SearchView { *; }

#-------------------------------------------------
# Facebook
#-------------------------------------------------
-keep class com.facebook.** { *; }

#-------------------------------------------------
# Gimbal
#-------------------------------------------------
-keep public interface com.gimbal.proguard.Keep {  }
-keep public class ** implements com.gimbal.proguard.Keep { *; }
-keep public class **.protocol.** { public protected *; }
-keepattributes Signature

-keep public class org.springframework.** { public protected *; }
-keep public class org.codehaus.jackson..** { public protected *; }
-dontwarn org.codehaus.**,com.fasterxml.**,org.simpleframework.**,com.google.**,org.apache.**

#-------------------------------------------------
# Gigya
#-------------------------------------------------
-keep class com.gigya.** { *; }
-keep interface com.gigya.** { *; }
-keep public class android.net.http.SslError
-keep public class android.webkit.WebViewClient
-dontwarn android.webkit.WebView
-dontwarn android.net.http.SslError
-dontwarn android.webkit.WebViewClient

#-------------------------------------------------
# Europa custom
#-------------------------------------------------
-keep class com.rmn.europa.data.api.responses.** { *; }
-keep class com.rmn.europa.data.api.** { *; }
-keep class com.rmn.europa.data.models.** { *; }
-keep class com.rmn.europa.data.generated.** { *; }

#-------------------------------------------------
# Google play services
#-------------------------------------------------
-keep public class com.google.android.gms.common.internal.safeparcel.SafeParcelable {
    public static final *** NULL;
}

-keepnames @com.google.android.gms.common.annotation.KeepName class *
-keepclassmembernames class * {
    @com.google.android.gms.common.annotation.KeepName *;
}

-dontnote com.google.android.apps.analytics.AdHitIdGenerator
-dontnote com.google.vending.licensing.ILicensingService
-dontnote com.android.vending.licensing.ILicensingService

-keep class * extends java.util.ListResourceBundle {
    protected Object[][] getContents();
}

#-------------------------------------------------
# Tune tracking
#-------------------------------------------------
-keep public class com.tune.** { public *; }
-keep public class com.google.android.gms.ads.identifier.** { *; }

#-------------------------
# Joda Time
#-------------------------
-keep class org.joda.time.tz.**  { *; }
-dontwarn javax.xml.bind.DatatypeConverter
-dontwarn org.joda.time.tz.ZoneInfoCompiler

#-------------------------
# Overlord Tracking
#-------------------------
-dontwarn com.rmn.commons.**
-dontwarn com.sun.activation.viewers.**
-dontwarn java.awt.**
-dontwarn java.beans.Beans
-dontwarn javax.activation.ActivationDataFlavor
-dontwarn javax.xml.bind.DatatypeConverter
-dontwarn org.apache.xmlbeans.**
-dontwarn org.mozilla.javascript.tools.debugger.**
-dontwarn org.mozilla.javascript.tools.shell.**
-dontwarn org.mozilla.javascript.xml.impl.xmlbeans.XML
-dontwarn sun.misc.Unsafe
-keep class com.github.fge.jsonschema.keyword.validator.** { *; }
-keep public class com.rmn.overlord.event.** {
    public *;
}

#-------------------------
# NewRelic
#-------------------------
-keep class com.newrelic.** { *; }
-dontwarn com.newrelic.**
-keepattributes Exceptions, Signature, InnerClasses, LineNumberTable

#-------------------------
# Button SDK
#-------------------------
-keepattributes Exceptions, InnerClasses, EnclosingMethod
-keep class com.usebutton.** { *; }
-keepclassmembers class * implements android.os.Parcelable {
    static ** CREATOR;
}
-keep class com.google.android.gms.ads.identifier.AdvertisingIdClient { public *; }

#-------------------------------------------------
# Misc warnings
#-------------------------------------------------
-dontnote com.google.android.apps.analytics.AdHitIdGenerator
-dontwarn org.w3c.dom.bootstrap.**
-dontwarn com.facebook.**
-dontwarn org.apache.commons.httpclient.**
-dontwarn org.simpleframework.xml.**
-dontwarn org.springframework.**
-dontwarn org.codehaus.jackson.**
-dontwarn com.parse.**
-dontwarn com.google.android.gms.**
-dontwarn sun.misc.Unsafe
-dontwarn okio.**
-dontwarn okhttp3.**
-dontwarn com.squareup.picasso.**
-dontwarn com.rmn.commons.**
-dontwarn org.apache.http.impl.client.HttpClients
-dontwarn org.apache.http.client.utils.HttpClientUtils
-dontwarn com.github.fge.**
